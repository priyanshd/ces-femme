define([
    'Magento_SalesRule/js/view/summary/discount',
    'jquery'
], function (Component, $) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Amasty_Coupons/summary/coupon'
        },

        initialize: function () {
            $(document).on('click', '.cart-summary tr[class="totals"]', function () {
                $(".total_coupons").toggle();
                $(this).find('.title').toggleClass('negative');
            });

            this._super();
        },

        getCoupons: function () {
            return this.amount;
        },

        /**
         * @override
         *
         * @returns {Boolean}
         */
        isDisplayed: function () {
            return this.getPureValue() != 0; //eslint-disable-line eqeqeq
        }
    });
});
