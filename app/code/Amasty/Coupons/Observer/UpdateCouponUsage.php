<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Observer;

use Magento\Framework\Event\ObserverInterface;

class UpdateCouponUsage implements ObserverInterface
{
    /**
     * @var \Magento\SalesRule\Model\Coupon
     */
    private $coupon;

    /**
     * @var \Magento\SalesRule\Model\ResourceModel\Coupon\Usage
     */
    private $couponUsage;

    /**
     * @var \Amasty\Coupons\Model\CouponRenderer
     */
    private $couponRenderer;

    /**
     * Save used coupon code ID
     *
     * @var
     */
    private $usedCodes = [];

    /**
     * @param \Magento\SalesRule\Model\Coupon $coupon
     * @param \Magento\SalesRule\Model\ResourceModel\Coupon\Usage $couponUsage
     */
    public function __construct(
        \Magento\SalesRule\Model\Coupon $coupon,
        \Magento\SalesRule\Model\ResourceModel\Coupon\Usage $couponUsage,
        \Amasty\Coupons\Model\CouponRenderer $couponRenderer
    ) {
        $this->coupon = $coupon;
        $this->couponUsage = $couponUsage;
        $this->couponRenderer = $couponRenderer;
    }

    /**
     * event sales_order_place_after
     *
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getEvent()->getOrder();
        if (!$order) {
            return $this;
        }
        // if order placement then increment else if order cancel then decrement
        $increment = (bool)$observer->getEventName() !== 'order_cancel_after';
        $customerId = $order->getCustomerId();
        $coupons = $this->couponRenderer->parseCoupon($order->getCouponCode());
        if (is_array($coupons) && count($coupons) > 1) {
            foreach ($coupons as $coupon) {
                if ($this->isUsed($coupon)) {
                    continue;
                }
                $this->coupon->load($coupon, 'code');
                if ($this->coupon->getId()) {
                    $this->coupon->setTimesUsed($this->coupon->getTimesUsed() + ($increment ? 1 : -1));
                    $this->coupon->save();
                    if ($customerId) {
                        $this->couponUsage->updateCustomerCouponTimesUsed(
                            $customerId,
                            $this->coupon->getId(),
                            $increment
                        );
                    }
                }
            }
        }

        return $this;
    }

    /**
     * @param string $code
     *
     * @return bool
     */
    private function isUsed($code)
    {
        if (!isset($this->usedCodes[$code])) {
            $this->usedCodes[$code] = 1;

            return false;
        }

        return true;
    }
}
