<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Observer;

use Magento\Framework\Event\ObserverInterface;

class FlushCouponObserver implements ObserverInterface
{
    /**
     * @var \Amasty\Coupons\Model\DiscountCollector
     */
    protected $discountCollector;

    /**
     * CouponObserver constructor.
     */
    public function __construct(
        \Amasty\Coupons\Model\DiscountCollector $discountCollector
    ) {
        $this->discountCollector = $discountCollector;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->discountCollector->flushAmount();
    }
}
