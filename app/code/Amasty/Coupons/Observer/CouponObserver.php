<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Observer;

use Magento\Framework\Event\ObserverInterface;

class CouponObserver implements ObserverInterface
{
    /**
     * @var \Amasty\Coupons\Model\DiscountCollector
     */
    protected $discountCollector;

    /**
     * CouponObserver constructor.
     * @param \Amasty\Coupons\Model\DiscountCollector $discountCollector
     */
    public function __construct(
        \Amasty\Coupons\Model\DiscountCollector $discountCollector
    ) {
        $this->discountCollector = $discountCollector;
    }

    /**
     * event salesrule_validator_process
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($observer->getData('rule')->getCode() != null) {
            $this->discountCollector->applyRuleAmount(
                $observer->getData('rule')->getCode(),
                $observer->getData('result')->getBaseAmount()
            );
        }
    }
}
