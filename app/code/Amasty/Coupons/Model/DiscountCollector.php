<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Model;

class DiscountCollector
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var array
     */
    protected $amount = [];

    /**
     * DiscountCollector constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->storeManager = $storeManager;
    }

    /**
     * Collect amount of discount for each rule
     * @param $ruleCode
     * @param $amount
     */
    public function applyRuleAmount($ruleCode, $amount)
    {
        if (!isset($this->amount[$ruleCode])) {
            $this->amount[$ruleCode] = 0;
        }

        $this->amount[$ruleCode] += $amount;
    }

    /**
     * Return amount of discount for each rule
     * @return array
     */
    public function getRulesWithAmount()
    {
        $totalAmount = [];
        foreach ($this->amount as $ruleCode => $ruleAmount) {
            $totalAmount[] = [
                'coupon_code'   => $ruleCode,
                'coupon_amount' =>
                    '-' .$this->storeManager->getStore()->getCurrentCurrency()->format($ruleAmount, [], false)
            ];
        }

        return $totalAmount;
    }

    public function flushAmount()
    {
        $this->amount = [];
    }
}
