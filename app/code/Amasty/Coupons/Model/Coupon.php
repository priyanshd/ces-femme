<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Model;

use Magento\Quote\Model\Quote\Address\Total\AbstractTotal;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address\Total;

class Coupon extends AbstractTotal
{
    const DISCOUNT_CODE = 'amasty_coupon_amount';

    /**
     * @var \Amasty\Coupons\Model\DiscountCollector
     */
    protected $discountCollector;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $session;

    /**
     * Coupon constructor.
     * @param DiscountCollector $discountCollector
     * @param \Magento\Checkout\Model\Session $session
     */
    public function __construct(
        \Amasty\Coupons\Model\DiscountCollector $discountCollector,
        \Magento\Checkout\Model\Session $session
    ) {
        $this->discountCollector = $discountCollector;
        $this->session = $session;
    }

    /**
     * Collect discount
     * @param Quote $quote
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment
     * @param Total $total
     */
    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
        parent::collect($quote, $shippingAssignment, $total);

        $this->setCouponsValues();
    }

    /**
     * Assign discount amount
     *
     * @param Quote $quote
     * @param Total $total
     *
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function fetch(Quote $quote, Total $total)
    {
        return [
            'code' => self::DISCOUNT_CODE,
            'title' => __('AmastyCoupon'),
            'value' => $this->session->getCouponAmount()
        ];
    }

    /**
     * Set coupons values into session
     * @return $this
     */
    public function setCouponsValues()
    {
        $this->session->setCouponAmount($this->discountCollector->getRulesWithAmount());

        return $this;
    }
}
