<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Model;

class Config extends \Amasty\Base\Model\ConfigProviderAbstract
{
    /**
     * xpath prefix of module (section)
     * @var string '{section}/'
     */
    protected $pathPrefix = 'amcoupons/';

    const UNIQUE_COUPONS = 'general/unique_codes';

    /**
     * @return string
     */
    public function getUniqueCoupons()
    {
        return $this->getValue(self::UNIQUE_COUPONS);
    }
}
