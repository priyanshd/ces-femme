<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Model;

class CouponRenderer
{
    /**
     * @var Config
     */
    private $configProvider;

    public function __construct(\Amasty\Coupons\Model\Config\Proxy $modelProvider)
    {
        $this->configProvider = $modelProvider;
    }

    /**
     * @param string $couponString
     *
     * @return array
     */
    public function parseCoupon($couponString)
    {
        if (!is_string($couponString)) {
            return [];
        }
        $coupons = array_unique(explode(',', $couponString));
        $result = [];
        foreach ($coupons as &$coupon) {
            $coupon = trim($coupon);
            if ($this->findCouponInArray($coupon, $result) === false) {
                $result[] = $coupon;
            }
        }

        return $result;
    }

    /**
     * @param string $couponString
     *
     * @return array|string
     */
    public function render($couponString)
    {
        $coupons = $this->parseCoupon($couponString);
        $uniqueIntersectArray = [];
        foreach ($coupons as $userCoupon) {
            if ($this->isCouponUnique($userCoupon)) {
                $uniqueIntersectArray[] = $userCoupon;
            }
        }
        if (!empty($uniqueIntersectArray)) {
            return end($uniqueIntersectArray);
        }

        return $coupons;
    }

    /**
     * @param string $coupon
     *
     * @return bool
     */
    public function isCouponUnique($coupon)
    {
        return $this->findCouponInArray($coupon, $this->getUniqueCoupons()) !== false;
    }

    /**
     * @param string $coupon
     * @param array $couponArray
     *
     * @return false|int
     */
    public function findCouponInArray($coupon, $couponArray)
    {
        if (!is_array($couponArray)) {
            return false;
        }
        foreach ($couponArray as $key => $code) {
            if (strcasecmp($coupon, $code) === 0) {
                return $key;
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public function getUniqueCoupons()
    {
        return $this->parseCoupon($this->configProvider->getUniqueCoupons());
    }
}
