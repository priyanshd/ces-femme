<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Plugin;

class RuleCollection
{
    /**
     * @var \Amasty\Coupons\Model\CouponRenderer
     */
    private $couponRenderer;

    /**
     * @var string
     */
    private $couponCode;

    /**
     * @var bool
     */
    private $isNeedToReplace = false;

    public function __construct(
        \Amasty\Coupons\Model\CouponRenderer $couponRenderer
    ) {
        $this->couponRenderer = $couponRenderer;
    }

    /**
     * @param \Magento\SalesRule\Model\ResourceModel\Rule\Collection $subject
     * @param int $websiteId
     * @param int $customerGroupId
     * @param string $couponCode
     * @param string|null $now
     * @param \Magento\Quote\Model\Quote\Address $address
     *
     * @return array|null
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeSetValidationFilter(
        \Magento\SalesRule\Model\ResourceModel\Rule\Collection $subject,
        $websiteId,
        $customerGroupId,
        $couponCode = '',
        $now = null,
        $address = null
    ) {
        $this->isNeedToReplace = false;
        if (!is_string($couponCode) || strpos($couponCode, ',') === false) {
            return null;
        }
        $this->couponCode = $couponCode;
        $couponCode = $this->couponRenderer->render($couponCode);
        if (is_string($couponCode)) {
            return [$websiteId, $customerGroupId, $couponCode, $now, $address];
        }
        $this->isNeedToReplace = true;
        return null;
    }

    /**
     * @param \Magento\SalesRule\Model\ResourceModel\Rule\Collection $subject
     * @param \Magento\SalesRule\Model\ResourceModel\Rule\Collection $result
     *
     * @return \Magento\SalesRule\Model\ResourceModel\Rule\Collection
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterSetValidationFilter(
        \Magento\SalesRule\Model\ResourceModel\Rule\Collection $subject,
        $result
    ) {
        if ($this->isNeedToReplace === false) {
            return $result;
        }

        $coupons = $this->couponRenderer->parseCoupon($this->couponCode);
        $connection = $result->getConnection();
        $select = $result->getSelect();
        $search = $connection->quoteInto(
            'code = ?',
            $this->couponCode
        );
        $replace = $connection->quoteInto(
            'code IN (?)',
            $coupons
        );
        $wherePart = $select->getPart(\Zend_Db_Select::WHERE);
        foreach ($wherePart as &$where) {
            $where = str_ireplace($search, $replace, $where);
        }
        $select->setPart(\Zend_Db_Select::WHERE, $wherePart);
        $select->group('rule_id');

        return $result;
    }
}
