<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Plugin;

class CouponPost
{
    const ONE_COUPON = 1;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $messageManager;

    /**
     * @var \Amasty\Coupons\Helper\Data
     */
    private $amHelper;

    /**
     * @var \Magento\Framework\Escaper
     */
    private $escaper;

    /**
     * Constructs a coupon read service object.
     *
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\Escaper $escaper
     * @param \Amasty\Coupons\Helper\Data $helper
     */
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Escaper $escaper,
        \Amasty\Coupons\Helper\Data $helper
    ) {
        $this->request = $request;
        $this->messageManager = $messageManager;
        $this->amHelper = $helper;
        $this->escaper = $escaper;
    }

    /**
     * @param \Magento\Checkout\Controller\Cart\CouponPost $subject
     * @param \Magento\Framework\Controller\Result\Redirect $back
     * @return mixed
     */
    public function afterExecute($subject, $back)
    {
        $appliedCodes = $this->amHelper->getRealAppliedCodes();
        $messages = $this->messageManager->getMessages();

        if (is_array($appliedCodes) && !empty($appliedCodes)) {
            /** @var \Magento\Framework\Message\Collection $messages */
            $messages->clear();
            $message = $this->messageManager->createMessage('success', 'amCoupons');
            $lastCode = trim($this->request->getParam('last_code'));
            $lastCodeArray = explode(',', $lastCode);
            $isRemoved = $this->request->getParam('remove_coupon');
            $messageText = __("You used coupon code \"%1\"", implode(",", $appliedCodes));
            $message->setText($messageText);
            $this->messageManager->getMessages()->addMessage($message);

            if (count($lastCodeArray) === self::ONE_COUPON && !in_array($lastCode, $appliedCodes)) {
                $messages->deleteMessageByIdentifier('amCoupons');
                if ($isRemoved) {
                    $this->messageManager->addSuccessMessage(
                        __(
                            'You canceled the coupon code "%1".',
                            $this->escaper->escapeHtml($lastCode)
                        )
                    );
                } else {
                    $this->messageManager->addErrorMessage(
                        __(
                            'The coupon code "%1" is not valid.',
                            $this->escaper->escapeHtml($lastCode)
                        )
                    );
                }
            } elseif (count($lastCodeArray) > self::ONE_COUPON) {
                $couponsDiff = array_diff($lastCodeArray, $appliedCodes);
                if (count($couponsDiff) !== 0) {
                    $this->messageManager->addErrorMessage(
                        __(
                            'Coupon code(s) not valid: %1',
                            $this->escaper->escapeHtml(implode(',', $couponsDiff))
                        )
                    );
                }
            }
        } elseif ($appliedCodes === false || empty($appliedCodes)) {
            $errors = $messages->getErrors();
            $messages->clear();
            foreach ($errors as $error) {
                preg_match('/"[^\"]*"/', $error->getText(), $couponCodes);
                if ($couponCodes) {
                    $this->messageManager->addErrorMessage(
                        __(
                            'Coupon code(s) not valid: %1',
                            $this->escaper->escapeHtml(str_replace('"', '', $couponCodes[0]))
                        )
                    );
                } else {
                    $this->messageManager->addErrorMessage($error->getText());
                }
            }
        }

        return $back;
    }
}
