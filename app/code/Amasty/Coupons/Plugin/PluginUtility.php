<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Plugin;

class PluginUtility
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    /**
     * @var \Magento\Framework\App\State
     */
    private $appState;

    /**
     * @var \Amasty\Coupons\Helper\Data
     */
    private $helper;

    /**
     * @var \Amasty\Coupons\Model\CouponRenderer
     */
    private $couponRenderer;

    /**
     * PluginUtility constructor.
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\App\State $appState,
        \Amasty\Coupons\Helper\Data $helper,
        \Amasty\Coupons\Model\CouponRenderer $couponRenderer
    ) {
        $this->request = $request;
        $this->appState = $appState;
        $this->helper = $helper;
        $this->couponRenderer = $couponRenderer;
    }

    /**
     * Check if rule can be applied for specific address/quote/custome
     * @param \Magento\SalesRule\Model\Utility $subject
     * @param \Closure $proceed
     * @param \Magento\SalesRule\Model\Rule $rule
     * @param \Magento\Quote\Model\Quote\Address $address
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundCanProcessRule(\Magento\SalesRule\Model\Utility $subject, \Closure $proceed, $rule, $address)
    {
        $coupons = $this->helper->getRealAppliedCodes(true, $address);
        $userKey = $this->couponRenderer->findCouponInArray($rule->getCode(), $coupons);
        if ($userKey !== false) {
            $originalCouponCode = $address->getQuote()->getCouponCode();
            if ($this->couponRenderer->isCouponUnique($rule->getCode())) {
                $address->getQuote()->setCouponCode($coupons[$userKey]);
            } else {
                $address->getQuote()->setCouponCode(implode(",", $coupons));
            }

            if ($proceed($rule, $address)) {
                //restore original coupon
                if ($this->appState->getAreaCode() == \Magento\Backend\App\Area\FrontNameResolver::AREA_CODE) {
                    $postParams = $this->request->getPost()->toArray();
                    $postParams['order']['coupon']['code'] = $address->getQuote()->getCouponCode();
                    $this->request->setPost(new \Zend\Stdlib\Parameters($postParams));
                }

                return true;
            } else {
                //restore original coupon
                $address->getQuote()->setCouponCode($originalCouponCode);
                return false;
            }
        }
        return $proceed($rule, $address);
    }
}
