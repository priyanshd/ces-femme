<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Block\Adminhtml\Order\Create\Coupons;

class Form extends \Magento\Sales\Block\Adminhtml\Order\Create\Coupons\Form
{
    /**
     * @return array|null
     */
    public function getCouponsCodes()
    {
        return $this->getCouponCode() ? explode(',', $this->getCouponCode()) : null;
    }

    /**
     * return onclick button js code
     *
     * @return string
     */
    public function getCouponJs()
    {
        $couponsString = '';
        if ($this->getCouponCode()) {
            $couponsString = '\'' . $this->getCouponCode() . ',\' + ';
        }
        $couponsString .= '$F(\'coupons:code\')';

        return 'order.applyCoupon(' . $couponsString . ')';
    }
}
