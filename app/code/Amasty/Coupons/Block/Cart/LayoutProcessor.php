<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Block\Cart;

use Magento\Checkout\Block\Checkout\LayoutProcessorInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class LayoutProcessor implements LayoutProcessorInterface
{
    /**
     * @var \Amasty\Coupons\Model\DiscountCollector
     */
    protected $discountCollector;

    /**
     * LayoutProcessor constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param \Amasty\Coupons\Model\DiscountCollector $discountCollector
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        \Amasty\Coupons\Model\DiscountCollector $discountCollector
    ) {
        $this->discountCollector = $discountCollector;
    }

    /**
     * @param array $jsLayout
     * @return array
     */
    public function process($jsLayout)
    {
        $couponsWithDiscount = $this->discountCollector->getRulesWithAmount();
        $jsLayout['components']['block-totals']['children']['before_grandtotal']['children']['coupon']['config']['amount']
            = $couponsWithDiscount;

        return $jsLayout;
    }
}
