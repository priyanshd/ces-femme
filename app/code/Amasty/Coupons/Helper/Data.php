<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Helper;

class Data
{
    /**
     * @var \Magento\SalesRule\Model\Coupon
     */
    private $couponModel;
    
    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $session;

    /**
     * @var \Magento\SalesRule\Model\ResourceModel\Coupon
     */
    private $couponResource;

    /**
     * @var \Amasty\Coupons\Model\CouponRenderer
     */
    private $couponRenderer;

    public function __construct(
        \Magento\Checkout\Model\Session\Proxy $session,
        \Magento\SalesRule\Model\Coupon $couponModel,
        \Magento\SalesRule\Model\ResourceModel\Coupon $couponResourceModel,
        \Amasty\Coupons\Model\CouponRenderer $couponRenderer,
        \Magento\Backend\Model\Session\Quote\Proxy $backendSession,
        \Magento\Framework\App\State $state
    ) {
        $this->couponModel = $couponModel;
        $this->session = $session;
        $this->couponResource = $couponResourceModel;
        $this->couponRenderer = $couponRenderer;
        $this->backendSession = $backendSession;
        $this->state = $state;
    }

    /**
     * @param bool $isRuleApplied
     * @param \Magento\Quote\Model\Quote\Address|null $address
     *
     * @return array|bool
     */
    public function getRealAppliedCodes($isRuleApplied = false, $address = null)
    {
        if ($address) {
            $quote = $address->getQuote();
        } else {
            $quote = $this->state->getAreaCode() === \Magento\Framework\App\Area::AREA_ADMINHTML
            ? $this->backendSession->getQuote()
            : $this->session->getQuote();
        }

        if (!$quote->getCouponCode()) {
            return [];
        }

        $coupons = $this->couponRenderer->parseCoupon($quote->getCouponCode());
        $appliedRules = array_map('trim', explode(',', $quote->getAppliedRuleIds()));

        if (!$appliedRules) {
            return false;
        }

        foreach ($coupons as $key => $coupon) {
            $rule = $this->couponModel->loadByCode($coupon);
            if (!$this->couponResource->exists($coupon)
                || (!$isRuleApplied && !in_array($rule->getRuleId(), $appliedRules))
            ) {
                unset($coupons[$key]);
            }
        }

        $coupons = array_unique($coupons);

        return $coupons;
    }
}
