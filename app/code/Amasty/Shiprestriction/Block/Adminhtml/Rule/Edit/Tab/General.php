<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Shiprestriction
 */

/**
 * Copyright © 2015 Amasty. All rights reserved.
 */
namespace Amasty\Shiprestriction\Block\Adminhtml\Rule\Edit\Tab;

use Amasty\Shiprestriction\Model\RegistryConstants;
use Amasty\CommonRules\Block\Adminhtml\Rule\Edit\Tab\General as CommonRulesGeneral;

class General extends CommonRulesGeneral
{
    /**
     * @var \Magento\Shipping\Model\Config\Source\Allmethods
     */
    private $allMethods;
    
    public function __construct(
        \Magento\Backend\Block\Template\Context $context, 
        \Magento\Framework\Registry $registry, 
        \Magento\Framework\Data\FormFactory $formFactory, 
        \Amasty\CommonRules\Model\OptionProvider\Pool $poolOptionProvider,
        \Magento\Shipping\Model\Config\Source\Allmethods $allMethods,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $poolOptionProvider, $data);
        $this->allMethods = $allMethods;
    }

    public function _construct()
    {
        $this->setRegistryKey(RegistryConstants::REGISTRY_KEY);
        parent::_construct();
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    protected function getLabel()
    {
        return __('Shipping Methods');
    }

    /**
     * @inheritdoc
     */
    protected function formInit($model)
    {
        $form = parent::formInit($model);

        $methods = $this->allMethods->toOptionArray();
        $methods[0]['label'] = __('(none)');
        $carriers = $this->poolOptionProvider->getOptionsByProviderCode('carriers');
        array_unshift($carriers, ['label' => __('(none)'), 'value' => '']);

        $fieldset = $form->getElement('apply_in');
        $fieldset->addField(
            'methods',
            'multiselect',
            [
                'label' => __('Restrict Shipping Methods'),
                'title' => __('Restrict Shipping Methods'),
                'name' => 'methods',
                'values' => $methods,
                'note' => __('Select methods you want to restrict'),
            ]
        );

        $fieldset->addField(
            'carriers',
            'multiselect',
            [
                'label' => __('Restrict ALL METHODS from Carriers'),
                'title' => __('Restrict ALL METHODS from Carriers'),
                'name' => 'carriers[]',
                'values' => $carriers,
                'note' => __('Select if you want to restrict ALL methods from the given carrirers'),
            ]
        );

        $fieldset->addField(
            'message',
            'text',
            [
                'label' => __('Error Message'),
                'title' => __('Error Message'),
                'name' => 'message',
            ]
        );

        return $form;
    }
}
