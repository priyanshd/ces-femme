<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Shiprestriction
 */


namespace Amasty\Shiprestriction\Model;

class Rule extends \Amasty\CommonRules\Model\Rule
{
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Amasty\CommonRules\Model\Rule\Condition\CombineFactory $conditionCombine,
        \Amasty\CommonRules\Model\Rule\Condition\Product\CombineFactory $conditionProductCombine,
        \Amasty\Base\Model\Serializer $serializer,
        \Amasty\CommonRules\Model\Modifiers\Subtotal $subtotalModifier,
        \Amasty\CommonRules\Model\Validator\Backorder $backorderValidator,
        \Amasty\Shiprestriction\Model\ResourceModel\Rule $resource,
        \Amasty\Shiprestriction\Model\Rule\Condition\CombineFactory $combineFactory,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $formFactory,
            $localeDate,
            $storeManager,
            $conditionCombine,
            $conditionProductCombine,
            $serializer,
            $subtotalModifier,
            $backorderValidator,
            $resource,
            $data
        );
        $this->conditionCombine = $combineFactory->create();
    }

    /**
     * _construct
     */
    protected function _construct()
    {
        $this->_init(\Amasty\Shiprestriction\Model\ResourceModel\Rule::class);
        parent::_construct();
        $this->subtotalModifier->setSectionConfig(\Amasty\Shiprestriction\Model\RegistryConstants::SECTION_KEY);
    }

    /**
     * @param \Magento\Quote\Model\Quote\Address\RateResult\Method $rate
     *
     * @return bool
     */
    public function restrict($rate)
    {
        $selectedCarriers = explode(',', $this->getCarriers());

        if (in_array($rate->getCarrier(), $selectedCarriers)) {
            return true;
        }
        $methods = $this->getMethods();

        if (!$methods) {
            return false;
        }
        $methods = array_unique(explode(',', $methods));
        $rateCode = $rate->getCarrier() . '_' . $rate->getMethod();

        /** @var string $pattern */
        foreach ($methods as $methodName) {
            if ($rateCode == $methodName) {
                return true;
            }
        }

        return false;
    }
}
