<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Shiprestriction
 */


namespace Amasty\Shiprestriction\Model;

class ShippingRestrictionRule
{
    const SECTION = "amshiprestriction";

    /**
     * @var \Magento\Framework\Model\Context
     */
    protected $context;

    protected $allRules;

    /**
     * @var \Amasty\Shiprestriction\Model\ResourceModel\Rule\Collection
     */
    protected $rulesCollection;

    /**
     * @var \Amasty\Shiprestriction\Model\ProductRegistry
     */
    protected $productRegistry;

    /**
     * @var Message\MessageBuilder
     */
    private $messageBuilder;

    /**
     * @var \Amasty\CommonRules\Model\Validator\SalesRule
     */
    private $salesRuleValidator;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Amasty\Shiprestriction\Model\ResourceModel\Rule\Collection $rulesCollection,
        \Amasty\Shiprestriction\Model\ProductRegistry $productRegistry,
        \Amasty\Shiprestriction\Model\Message\MessageBuilder $messageBuilder,
        \Amasty\CommonRules\Model\Validator\SalesRule $salesRuleValidator
    ) {
        $this->context = $context;
        $this->rulesCollection = $rulesCollection;
        $this->productRegistry = $productRegistry;
        $this->messageBuilder = $messageBuilder;
        $this->salesRuleValidator = $salesRuleValidator;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $request
     *
     * @return array
     */
    public function getRestrictionRules($request)
    {
        /** @var \Magento\Quote\Model\Quote\Item[] $allItems */
        $allItems = $request->getAllItems();

        if (!$allItems) {
            return [];
        }

        $firstItem = current($allItems);
        /** @var \Magento\Quote\Model\Quote\Address $address */
        $address = $firstItem->getAddress();
        $address->setItemsToValidateRestrictions($allItems);

        //multishipping optimization
        if (is_null($this->allRules)) {
            $this->allRules = $this->rulesCollection->addAddressFilter($address);

            if ($this->isAdmin()) {
                $this->allRules->addFieldToFilter('for_admin', 1);
            }

            $this->allRules->load();

            /** @var \Amasty\Shiprestriction\Model\Rule $rule */
            foreach ($this->allRules as $rule) {
                $rule->afterLoad();
            }
        }

        /**
         * Fix for admin checkout
         *
         * UPD: Return missing address data (discount, grandtotal, etc)
         */
        if ($this->isAdmin()) {
            $address->addData($address->getOrigData());
        }

        // remember old
        $subtotal = $address->getSubtotal();
        $baseSubtotal = $address->getBaseSubtotal();
        $validRules = $this->getValidRules($address, $allItems);
        // restore
        $address->setSubtotal($subtotal);
        $address->setBaseSubtotal($baseSubtotal);

        return $validRules;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Address $address
     * @param \Magento\Quote\Model\Quote\Item[] $allItems
     *
     * @return \Amasty\Shiprestriction\Model\Rule[]
     */
    private function getValidRules($address, $allItems)
    {
        $validRules = [];
        /** @var \Amasty\Shiprestriction\Model\Rule $rule */
        foreach ($this->allRules as $rule) {
            $this->productRegistry->clearProducts();

            if ($rule->validate($address, $allItems)
                && $this->salesRuleValidator->validate($rule, $allItems)
            ) {
                // remember used products
                $newMessage = $this->messageBuilder->parseMessage(
                    $rule->getMessage(),
                    $this->productRegistry->getProducts()
                );

                $rule->setMessage($newMessage);
                $validRules[] = $rule;
            }
        }

        return $validRules;
    }

    /**
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function isAdmin()
    {
        return $this->context->getAppState()->getAreaCode() == \Magento\Backend\App\Area\FrontNameResolver::AREA_CODE;
    }
}
