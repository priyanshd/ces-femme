require([
    'jquery',
    'mage/url',
    'underscore'
], function ($, urlBuilder, _) {
    'use strict';
    $('[data-amfeed-js="amfeed-taxonomy-category"]').click(function () {
        $('[data-amfeed-js="amfeed-taxonomy-input"]').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: urlBuilder.build('/admin/amfeed/googleWizard/search'),
                    data: {
                        category: request.term,
                        source: $('#feed_googlewizard_categories_taxonomy_source :selected').val()
                    },

                    success: function (result) {
                        response(result);
                    }
                });
            },
            appendTo: '[data-amfeed-js="amfeed-category-list"]',
            messages: {
                results: function () {}
            }
        });
    });
});