<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Rgrid
 */

namespace Amasty\Rgrid\Controller\Adminhtml\Promo;

abstract class Quote extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\SalesRule\Model\ResourceModel\Rule\CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @var \Magento\SalesRule\Api\RuleRepositoryInterface
     */
    protected $ruleRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $criteriaBuilder;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\SalesRule\Model\ResourceModel\Rule\CollectionFactory $collectionFactory,
        \Magento\SalesRule\Api\RuleRepositoryInterface $ruleRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $criteriaBuilder
    ) {
        parent::__construct($context);
        $this->_collectionFactory = $collectionFactory;
        $this->ruleRepository = $ruleRepository;
        $this->criteriaBuilder = $criteriaBuilder;
    }

    /**
     * @param array $ruleIds
     *
     * @return \Magento\SalesRule\Api\Data\RuleSearchResultInterface
     */
    protected function getByRuleIds($ruleIds)
    {
        $criteria = $this->getCriteriaBuilder($ruleIds);

        return $this->ruleRepository->getList($criteria);
    }

    /**
     * @param $ruleIds
     *
     * @return \Magento\Framework\Api\SearchCriteria
     */
    private function getCriteriaBuilder($ruleIds)
    {
        return $this->criteriaBuilder->addFilter(
            \Magento\SalesRule\Model\Data\Rule::KEY_RULE_ID,
            $ruleIds,
            'in'
        )
            ->create();
    }
}
