<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Rgrid
 */

namespace Amasty\Rgrid\Controller\Adminhtml\Promo\Quote;

class MassPriority extends \Amasty\Rgrid\Controller\Adminhtml\Promo\Quote
{
    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $ids = $this->getRequest()->getParam('ids');
        $priority = $this->getRequest()->getParam('priority');

        $collection = $this->_collectionFactory->create();
        if ($priority == 'low') {
            $rulePriority = $collection->setOrder('sort_order', 'DESC')->setPageSize(1)->getFirstItem()->getSortOrder();
            $rulePriority++;
        } else {
            $rulePriority = $collection->setOrder('sort_order', 'ASC')->setPageSize(1)->getFirstItem()->getSortOrder();
            if ($rulePriority != 0) {
                $rulePriority--;
            }
        }

        if ($ids) {
            try {
                $rules = $this->getByRuleIds($ids);

                /** @var \Magento\SalesRule\Model\Rule $rule */
                foreach ($rules->getItems() as $rule) {
                    $rule->setSortOrder($rulePriority);
                    $this->ruleRepository->save($rule);
                }

                $this->messageManager->addSuccessMessage(
                    __('A total of %1 record(s) have been updated.', $rules->getTotalCount())
                );
                $this->_redirect('sales_rule/*/');
                return;
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(
                    __('Something went wrong while updating the rule(s) status.')
                );
            }
        }

        $this->_redirect('sales_rule/*/');
    }
}
