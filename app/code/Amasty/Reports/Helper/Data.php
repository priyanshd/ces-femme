<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Reports
 */


namespace Amasty\Reports\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{

    /**
     * @var \Magento\Backend\Model\Session
     */
    private $session;

    /**
     * Wrapped component
     *
     * @var \Magento\Ui\Component\Form\Element\DataType\Date
     */
    private $wrappedComponent;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Backend\Model\Session $session,
        \Magento\Ui\Component\Form\Element\DataType\Date $wrappedComponent
    ) {
        parent::__construct($context);
        $this->session = $session;
        $this->wrappedComponent = $wrappedComponent;
    }

    public function getDefaultFromDate()
    {
        return strtotime('-7 day');
    }

    public function getDefaultToDate()
    {
        return time();
    }

    public function getCurrentStoreId()
    {
        return $this->session->getAmreportsStore();
    }

    public function setCurrentStore($store)
    {
        return $this->session->setAmreportsStore($store);
    }

    /**
     * Getting time according to locale
     *
     * @param string|Date $date
     * @param int $hour
     * @param int $minute
     * @param int $second
     * @return string
     */
    public function getDateForLocale($date, $hour = 0, $minute = 0, $second = 0)
    {
        $skipTimeZoneConversion = $this->scopeConfig->getValue(
            'config/skipTimeZoneConversion',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        return $this->wrappedComponent->convertDate(
            $date,
            $hour,
            $minute,
            $second,
            !$skipTimeZoneConversion
        )->format('Y-m-d H:i:s');
    }
}
