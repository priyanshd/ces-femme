<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_BannersLite
 */


namespace Amasty\BannersLite\Model;

use Amasty\BannersLite\Api\Data\BannerRuleInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Framework\DB\Adapter\AdapterInterface;

class Cache
{
    /**
     * @var \Magento\PageCache\Model\Cache\Type $cache
     */
    private $cache;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var \Magento\Catalog\Model\CategoryFactory
     */
    private $categoryFactory;

    /**
     * @var ResourceModel\CategoryProduct
     */
    private $categoryProduct;

    public function __construct(
        \Magento\PageCache\Model\Cache\Type $cache,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryFactory,
        \Amasty\BannersLite\Model\ResourceModel\CategoryProduct $categoryProduct
    ) {
        $this->cache = $cache;
        $this->collectionFactory = $collectionFactory;
        $this->categoryFactory = $categoryFactory;
        $this->categoryProduct = $categoryProduct;
    }

    /**
     * @param array $bannerRule
     */
    public function cleanProductCache($bannerRule)
    {
        $this->cache->clean(
            \Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG,
            array_unique($this->getCacheTags($bannerRule))
        );
    }

    /**
     * @param array $bannerRule
     *
     * @return array
     */
    private function getCacheTags($bannerRule)
    {
        switch ($bannerRule[BannerRuleInterface::SHOW_BANNER_FOR]) {
            case BannerRuleInterface::PRODUCT_SKU:
                $cacheTags = $this->getProductCacheTags($bannerRule);
                break;

            case BannerRuleInterface::PRODUCT_CATEGORY:
                $cacheTags = $this->getCategoryCacheTags($bannerRule);
                break;

            case BannerRuleInterface::ALL_PRODUCTS:
                $cacheTags = [\Magento\Catalog\Model\Product::CACHE_TAG];
                break;

            default:
                $cacheTags = [];
                break;
        }

        return $cacheTags;
    }

    /**
     * @param array $bannerRule
     *
     * @return array
     */
    private function getProductCacheTags($bannerRule)
    {
        /** @var Collection $productCollection */
        $productCollection = $this->collectionFactory->create();

        $productCollection->addFieldToFilter(
            ProductInterface::SKU,
            ['in' => $bannerRule[BannerRuleInterface::BANNER_PRODUCT_SKU]]
        );

        return $this->getTagsByProductIds($productCollection->getAllIds());
    }

    /**
     * @param array $bannerRule
     *
     * @return array
     */
    private function getCategoryCacheTags($bannerRule)
    {
        return $this->getTagsByProductIds($this->categoryProduct->getProductIds($bannerRule));
    }

    /**
     * @param array $productIds
     *
     * @return array
     */
    private function getTagsByProductIds($productIds)
    {
        $cacheTags = [];

        foreach ($productIds as $productId) {
            $cacheTags[] = \Magento\Catalog\Model\Product::CACHE_TAG . '_' . $productId;
        }

        return $cacheTags;
    }
}
