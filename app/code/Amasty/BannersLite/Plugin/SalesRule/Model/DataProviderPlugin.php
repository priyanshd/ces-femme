<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_BannersLite
 */


namespace Amasty\BannersLite\Plugin\SalesRule\Model;

use Amasty\BannersLite\Api\Data\BannerInterface;
use Amasty\BannersLite\Model\ImageProcessor;
use Amasty\Base\Model\Serializer;

class DataProviderPlugin
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var ImageProcessor
     */
    private $imageProcessor;

    public function __construct(Serializer $serializer, ImageProcessor $imageProcessor)
    {
        $this->serializer = $serializer;
        $this->imageProcessor = $imageProcessor;
    }

    /**
     * Convert Promo Banners data to Array
     *
     * @param \Magento\SalesRule\Model\Rule\DataProvider $subject
     * @param array $result
     *
     * @return array
     */
    public function afterGetData(\Magento\SalesRule\Model\Rule\DataProvider $subject, $result)
    {
        if (is_array($result)) {
            foreach ($result as &$item) {
                if (isset($item[BannerInterface::EXTENSION_ATTRIBUTES_KEY][BannerInterface::EXTENSION_CODE])) {
                    $banners = &$item[BannerInterface::EXTENSION_ATTRIBUTES_KEY][BannerInterface::EXTENSION_CODE];
                    foreach ($banners as $key => $banner) {
                        /** @var \Amasty\BannersLite\Model\Banner $banner */
                        if ($banner instanceof BannerInterface) {
                            $banners[$key] = $this->convertBannerToArray($banner);
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param \Amasty\BannersLite\Model\Banner $banner
     *
     * @return array
     */
    private function convertBannerToArray(\Amasty\BannersLite\Model\Banner $banner)
    {
        $array = $banner->toArray();

        if ($array[BannerInterface::BANNER_IMAGE]
            && is_string($array[BannerInterface::BANNER_IMAGE])
        ) {
            $array[BannerInterface::BANNER_IMAGE]
                = $this->serializer->unserialize($array[BannerInterface::BANNER_IMAGE]);
            $array[BannerInterface::BANNER_IMAGE][0]['url']
                = $this->imageProcessor->getBannerImageUrl($array[BannerInterface::BANNER_IMAGE][0]['name']);
        }

        return $array;
    }
}
