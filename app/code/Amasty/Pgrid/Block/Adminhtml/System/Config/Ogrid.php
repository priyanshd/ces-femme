<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Pgrid
 */


namespace Amasty\Pgrid\Block\Adminhtml\System\Config;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Module\Manager;

class Ogrid extends Field
{
    /**
     * @var Manager
     */
    private $manager;

    public function __construct(
        Manager $manager,
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        $this->manager = $manager;
        parent::__construct($context, $data);
    }

    public function render(AbstractElement $element)
    {
        if ($this->manager->isEnabled('Amasty_Ogrid')) {
            $element->setValue(__('Installed'));
            $element->setHtmlId('amasty_is_instaled');
            $url = $this->getUrl('adminhtml/system_config/edit', ['section' => 'amasty_ogrid']);
            $element->setComment(__('Specify Extended Order Grid settings properly See more details '
                . '<a href="%1" target="_blank">here</a>', $url
            ));
        } else {
            $element->setValue(__('Not Installed'));
            $element->setHtmlId('amasty_not_instaled');
            $element->setComment(__('Increase your order grid functionality - add a customer, product, shipping '
                . 'or billing attributes to see only relevant order info and rapidly search for orders. '
                . 'See more details <a href="https://amasty.com/extended-order-grid-for-magento-2.html'
                . '?utm_source=extension&utm_medium=backend&utm_campaign=from_pgrid_to_ogrid_m2" target="_blank">here.</a>'
            ));
        }

        return parent::render($element);
    }
}
