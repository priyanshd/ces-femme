<?php

namespace Trooperlab\CategoryPage\Block\Magento\Catalog\Product;

use Magento\Catalog\Helper\ImageFactory as HelperFactory;

class ImageBuilder extends \Magento\Catalog\Block\Product\ImageBuilder
{

    /**
     * @param HelperFactory $helperFactory
     * @param \Magento\Catalog\Block\Product\ImageFactory $imageFactory
     */
    public function __construct(
        HelperFactory $helperFactory,
        \Magento\Catalog\Block\Product\ImageFactory $imageFactory
    ) {
        parent::__construct($helperFactory, $imageFactory);
    }

    /**
     * Create image block
     *
     * @return \Magento\Catalog\Block\Product\Image
     */
    public function create()
    {
        $hoverImageIds = [];

        /** Check if product listing hover is enabled */
            $hoverImageIds[] = 'category_page_grid';
            $hoverImageIds[] = 'category_page_list';
            $hoverImageIds[] = 'new_products_content_widget_grid';
            $hoverImageIds[] = 'recently_viewed_products_grid_content_widget';


        /** @var \Magento\Catalog\Helper\Image $helper */
        $helper = $this->helperFactory->create()
            ->init($this->product, $this->imageId);

        $template = $helper->getFrame()
            ? 'Magento_Catalog::product/image.phtml'
            : 'Magento_Catalog::product/image_with_borders.phtml';

        $data['data']['template'] = $template;

        $imagesize = $helper->getResizedImageInfo();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $mediaurl = $objectManager->get('Magento\Store\Model\StoreManagerInterface')
                    ->getStore()
                    ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

        $image_url = explode('pub/media',$helper->getUrl());
        $image_url = $mediaurl. $image_url[1];
        $data = [
            'data' => [
                'template' => $template,
                'image_url' => $image_url,
                'width' => $helper->getWidth(),
                'height' => $helper->getHeight(),
                'label' => $helper->getLabel(),
                'ratio' =>  $this->getRatio($helper),
                'custom_attributes' => $this->getCustomAttributes(),
                'resized_image_width' => !empty($imagesize[0]) ? $imagesize[0] : $helper->getWidth(),
                'resized_image_height' => !empty($imagesize[1]) ? $imagesize[1] : $helper->getHeight(),
            ],
        ];





        if (in_array($this->imageId, $hoverImageIds)) {
            /** @var \Magento\Catalog\Helper\Image $helper */
            $hoverHelper = $this->helperFactory->create()
                ->init($this->product, $this->imageId . '_hover')
						->constrainOnly(true)
                        ->keepAspectRatio(true)
                        ->keepTransparency(true)
                        ->keepFrame(false)->resize($helper->getWidth(), $helper->getHeight());

            $hoverImageUrl = explode('pub/media', $hoverHelper->getUrl());
            if(isset($hoverImageUrl[1])){
                $hoverImageUrl = $mediaurl. $hoverImageUrl[1];
            }else{
                $hoverImageUrl = $hoverHelper->getUrl();
            }
            $placeHolderUrl =  $hoverHelper->getDefaultPlaceholderUrl();

            /** Do not display hover placeholder */
            if ($placeHolderUrl == $hoverImageUrl) {
                $data['data']['hover_image_url'] = NULL;
            } else {
                $data['data']['hover_image_url'] = $hoverImageUrl;
            }
        }


        return $this->imageFactory->create($data);
    }
}

