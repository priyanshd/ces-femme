<?php


namespace Trooperlab\PreOrder\Observer\Frontend\Catalog;

class ControllerProductView implements \Magento\Framework\Event\ObserverInterface
{

	/**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * __construct function
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        $this->_objectManager = $objectManager;
    }
	

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
		$product = $observer->getEvent()->getData('product');
		
		if($product->getHideForGuest()){
			$customerSession = $this->_objectManager->get('\Magento\Customer\Model\Session');
			$urlInterface = $this->_objectManager->get('\Magento\Framework\UrlInterface');
			if(!$customerSession->isLoggedIn()) {
				$customerSession->setAfterAuthUrl($urlInterface->getCurrentUrl());
				$customerSession->authenticate();
			}		
		}
    }
}