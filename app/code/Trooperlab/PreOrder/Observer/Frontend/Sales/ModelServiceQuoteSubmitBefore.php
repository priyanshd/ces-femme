<?php


namespace Trooperlab\PreOrder\Observer\Frontend\Sales;

class ModelServiceQuoteSubmitBefore implements \Magento\Framework\Event\ObserverInterface
{

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        $quote = $observer->getQuote();
		$order = $observer->getOrder();

		
        $CustomFeePreorder = $quote->getPreorder();
        $CustomFeeBasePreorder = $quote->getBasePreorder();

        if ($CustomFeePreorder&&$CustomFeeBasePreorder) {

			$order->setData('preorder', $CustomFeePreorder);
			$order->setData('base_preorder', $CustomFeeBasePreorder);

        }


        return $this;
    }
}
