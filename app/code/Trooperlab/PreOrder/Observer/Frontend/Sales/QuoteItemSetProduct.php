<?php


namespace Trooperlab\PreOrder\Observer\Frontend\Sales;

class QuoteItemSetProduct implements \Magento\Framework\Event\ObserverInterface
{

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        $product = $observer->getProduct();
        $quoteItem = $observer->getQuoteItem();
        $quoteItem = ( $quoteItem->getParentItem() ? $quoteItem->getParentItem() : $quoteItem );
        $productid = $quoteItem->getProductId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $StockState = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');
        $stock      = $StockState->getStockQty($productid, $storeManager->getStore()->getWebsiteId());
        $product = $objectManager->create('Magento\Catalog\Model\Product')->load($quoteItem->getProductId());
        if($stock<=0 && (int)$product->getIsPreordered()){
            $quoteItem->setPreorderPrice($quoteItem->getPrice());
            //$quoteItem->setPreorderPrice('-'.$quoteItem->getPrice());
            // $quoteItem->setPrice('0');
            // $quoteItem->setBasePrice('0');
            // $quoteItem->setCustomPrice('0');
            // $quoteItem->setRowTotal('0');
            // $quoteItem->setBaseRowTotal('0');
            // $quoteItem->setPriceInclTax('0');
            // $quoteItem->setBasePriceInclTax('0');
            // $quoteItem->setRowTotalInclTax('0');
            // $quoteItem->setBaseRowTotalInclTax('0');
            $quoteItem->setIsPreordered(1);
            $arrivaldate = date("d-m-Y",strtotime($product->getData('arrival_date')));
            $quoteItem->setArrivalDate($arrivaldate);
        }
    }
}
