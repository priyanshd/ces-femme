<?php
    /**
     * Webkul Hello CustomPrice Observer
     *
     * @category    Webkul
     * @package     Webkul_Hello
     * @author      Webkul Software Private Limited
     *
     */
    namespace Trooperlab\PreOrder\Observer\Frontend\Checkout;
 
    use Magento\Framework\Event\ObserverInterface;
    use Magento\Framework\App\RequestInterface;
 
    class CartProductAddAfter implements ObserverInterface
    {
        public function execute(\Magento\Framework\Event\Observer $observer) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $item = $observer->getEvent()->getData('quote_item');         
            $quoteItem = ( $item->getParentItem() ? $item->getParentItem() : $item );
            $productid = $quoteItem->getProductId();
            $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
            $StockState = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');
            $stock      = $StockState->getStockQty($productid, $storeManager->getStore()->getWebsiteId());
            $product = $objectManager->create('Magento\Catalog\Model\Product')->load($quoteItem->getProductId());
            if($stock<=0 && (int)$product->getIsPreordered()){

                $price = 0; //set your price here
                // $quoteItem->setPreorderPrice($quoteItem->getPrice());
                // $quoteItem->setCustomPrice($price);
                // $quoteItem->setOriginalCustomPrice($price);
                // $quoteItem->getProduct()->setIsSuperMode(true);
            }




            
        }
 
    }
