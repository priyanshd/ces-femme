<?php

namespace Trooperlab\PreOrder\Model\Total;

use Magento\Store\Model\ScopeInterface;

class Preorder extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{

    protected $helperData;

    protected $quoteValidator = null;

    public function __construct(\Magento\Quote\Model\QuoteValidator $quoteValidator,
		\Trooperlab\PreOrder\Helper\Data $helperData)
    {
        $this->quoteValidator = $quoteValidator;
        $this->helperData = $helperData;
    }

    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    )
    {
        parent::collect($quote, $shippingAssignment, $total);
        if (!count($shippingAssignment->getItems())) {
            return $this;
        }

        $enabled = $this->helperData->isModuleEnabledPreorder();
        $minimumOrderAmount = $this->helperData->getMinimumOrderAmountPreorder();
        $subtotal = $total->getTotalAmount('subtotal');
        if ($enabled && $minimumOrderAmount <= $subtotal) {
            $preorder = $quote->getPreorder();
            $total->setTotalAmount('preorder', $preorder);
            $total->setBaseTotalAmount('preorder', $preorder);
            $total->setPreorder($preorder);
           // $total->setBasePreorder($preorder);
           // $quote->setPreorder($preorder);
           // $quote->setBasePreorder($preorder);
           // $total->setGrandTotal($total->getGrandTotal() + $preorder);
           // $total->setBaseGrandTotal($total->getBaseGrandTotal() + $preorder);
        }
        return $this;
    }

    public function fetch(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Address\Total $total)
    {

        $enabled = $this->helperData->isModuleEnabledPreorder();
        $minimumOrderAmount = $this->helperData->getMinimumOrderAmountPreorder();
        $subtotal = $quote->getSubtotal();
        //$preorder = $quote->getPreorder();
        if ($enabled && $minimumOrderAmount <= $subtotal && $preorder) {
            return [
                'code' => 'preorder',
                'title' => 'Pre-Order Total',
                'value' => $preorder
            ];
        } else {
            return array();
        }
    }

    public function getLabel()
    {
        return __('Pre-Order Total');
    }

    protected function clearValues(\Magento\Quote\Model\Quote\Address\Total $total)
    {
        $total->setTotalAmount('subtotal', 0);
        $total->setBaseTotalAmount('subtotal', 0);
        $total->setTotalAmount('tax', 0);
        $total->setBaseTotalAmount('tax', 0);
        $total->setTotalAmount('discount_tax_compensation', 0);
        $total->setBaseTotalAmount('discount_tax_compensation', 0);
        $total->setTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setBaseTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setSubtotalInclTax(0);
        $total->setBaseSubtotalInclTax(0);

    }
}