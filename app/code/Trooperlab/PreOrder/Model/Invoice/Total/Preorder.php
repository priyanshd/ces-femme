<?php

namespace Trooperlab\PreOrder\Model\Invoice\Total;

use Magento\Sales\Model\Order\Invoice\Total\AbstractTotal;

class Preorder extends AbstractTotal {

	public function collect(\Magento\Sales\Model\Order\Invoice $invoice) {
		$order = $invoice->getOrder();
		$percent = $invoice->getSubtotal() / $order->getSubtotal();
		$invoice->setPreorder(0);
		$invoice->setBasePreorder(0);
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		if(isset($_POST) && !empty($_POST['invoice']['items']))
		{
			$amount = 0;
			foreach ($_POST['invoice']['items'] as $key => $value) {
				$order_item = $objectManager->create('Magento\Sales\Model\Order\Item')->load($key);
				$productId 	= $order_item->getData('product_id');
				$product = $objectManager->create('Magento\Catalog\Model\Product')->load($productId);
    	        if($order_item->getIsPreordered()==1){
    	         $amount = $amount+($value*$order_item->getPrice());
    	        }
			}
			$amount = $amount;
			$baseAmount = $amount;
		} 
		else 
		{
			$inamount = 0;
			foreach ($invoice->getAllItems() as $value) {
				$order_items = $objectManager->create('Magento\Sales\Model\Order\Item')->load($value->getOrderItemId());
				if($order_items->getIsPreordered()==1){
                 $inamount = $inamount+($order_items->getQtyOrdered() - $order_items->getQtyInvoiced())*$order_items->getPrice();
				}
			}
			if($inamount){
            $amount = $inamount;
			$baseAmount = $inamount;
			}else{
			$amount = -($order->getPreorder());
			$baseAmount = -($order->getPreorder());
		}
	}
		$invoice->setPreorder($amount);
		$invoice->setBasePreorder($baseAmount);
		
		$invoice->setBaseSubTotal($order->getSubtotal()-$amount);
		$invoice->setGrandTotal($invoice->getGrandTotal());
		$invoice->setBaseGrandTotal($invoice->getBaseGrandTotal());
		return $this;
	} 
} 