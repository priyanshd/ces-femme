<?php

namespace Trooperlab\PreOrder\Model\Quote\Total;

use Magento\Store\Model\ScopeInterface;

class Preorder extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal {

	protected $helperData;

	protected $quoteValidator = null;

	public function __construct(
		\Magento\Quote\Model\QuoteValidator $quoteValidator,
		\Trooperlab\PreOrder\Helper\Data $helperData
	) {
		$this->quoteValidator = $quoteValidator;
		$this->helperData = $helperData;
	} 

	public function collect(\Magento\Quote\Model\Quote $quote,
		\Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
		\Magento\Quote\Model\Quote\Address\Total $total
		) {
		parent::collect($quote, $shippingAssignment, $total);
		if (!count($shippingAssignment->getItems())) {
			return $this;
		} 

		$enabled = $this->helperData->isModuleEnabledPreorder();
		$minimumOrderAmount = $this->helperData->getMinimumOrderAmountPreorder();
		$subtotal = $total->getTotalAmount('subtotal');
		if ($enabled && $minimumOrderAmount <= $subtotal) {
			$preorder=$this->helperData->getCustomFeePreorder();
			// Try to test with sample value
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $cart          = $objectManager->get('\Magento\Checkout\Model\Cart'); 
            $items         = $cart->getQuote()->getAllVisibleItems(); 
            $sum           = 0; 
			$subtotalshow  = 0;
			$arra = array();
			foreach($items as $item) {
				$product = $item->getProduct();
				$arra[$item->getId()] = array('ispreorder'=>$item->getIsPreordered(),'name'=>$item->getName());
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $StockState = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');
                $product = $objectManager->get('Magento\Catalog\Model\Product')->load($product->getId());
                $stock      = $StockState->getStockQty($item->getProductId(), $product->getStore()->getWebsiteId());
                if($stock<=0 && (int)$item->getIsPreordered()){
                   $sum +=  $item->getPrice() * $item->getQty();
                }
				$subtotalshow +=  $item->getPrice() * $item->getQty();
				
        	}
        	$preorderpre = number_format($sum, 2, '.', '');
        	$preorder = '-'.$preorderpre;
		
			$total->setTotalAmount('preorder', $preorder);
			$total->setBaseTotalAmount('preorder', $preorder);
			$total->setPreorder($preorder);
			$total->setBasePreorder($preorder);
			$quote->setPreorder($preorder);
			$quote->setBasePreorder($preorder); 

		} 
		return $this;
	} 

	public function fetch(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Address\Total $total) {
		$enabled = $this->helperData->isModuleEnabledPreorder();
		$minimumOrderAmount = $this->helperData->getMinimumOrderAmountPreorder();
		$subtotal = $quote->getSubtotal();
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $cart          = $objectManager->get('\Magento\Checkout\Model\Cart'); 
            $items         = $cart->getQuote()->getAllItems(); 
            $sum           = 0; 
		foreach($items as $item) {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $StockState = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');
                $product = $item->getProduct();
				$product = $objectManager->get('\Magento\Catalog\Model\Product')->load($product->getId());
                $stock      = $StockState->getStockQty($item->getProductId(), $product->getStore()->getWebsiteId());
                if($stock<=0 && (int)$item->getIsPreordered()){
                   $sum +=  $item->getPrice()*$item->getQty();
                }
        	}
        	$preorder = number_format($sum, 2, '.', '');

		$result = [];
		if ($enabled && ($minimumOrderAmount <= $subtotal) && $preorder) {
			$result = [
			'code' => 'preorder',
			'title' => $this->helperData->getFeeLabelPreorder(),
			'value' => '-'.$preorder
			];
		} 
		return $result;
	} 

	public function getLabel() {
		return __('Pre-Order Total');
	} 

	protected function clearValues(\Magento\Quote\Model\Quote\Address\Total $total) {
		$total->setTotalAmount('subtotal', 0);
		$total->setBaseTotalAmount('subtotal', 0);
		$total->setTotalAmount('tax', 0);
		$total->setBaseTotalAmount('tax', 0);
		$total->setTotalAmount('discount_tax_compensation', 0);
		$total->setBaseTotalAmount('discount_tax_compensation', 0);
		$total->setTotalAmount('shipping_discount_tax_compensation', 0);
		$total->setBaseTotalAmount('shipping_discount_tax_compensation', 0);
		$total->setSubtotalInclTax(0);
		$total->setBaseSubtotalInclTax(0);
	} 
}  