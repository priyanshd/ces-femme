<?php

namespace Trooperlab\PreOrder\Model\Magento\SalesRule;

use Magento\Quote\Model\Quote\Item\AbstractItem;

class Validator extends \Magento\SalesRule\Model\Validator
{
	public function process(AbstractItem $item)
    {
        $item->setDiscountAmount(0);
        $item->setBaseDiscountAmount(0);
        $item->setDiscountPercent(0);

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $qitem = $objectManager->create('Magento\Quote\Model\Quote\Item')->load($item->getItemId());
         if($qitem->getIsPreordered()==1){
            return $this;
         }

        $itemPrice = $this->getItemPrice($item);
        if ($itemPrice < 0) {
            return $this;
        }

        $appliedRuleIds = $this->rulesApplier->applyRules(
            $item,
            $this->_getRules($item->getAddress()),
            $this->_skipActionsValidation,
            $this->getCouponCode()
        );
        $this->rulesApplier->setAppliedRuleIds($item, $appliedRuleIds);

        return $this;
    }
}
	
	