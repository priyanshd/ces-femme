<?php

namespace Trooperlab\PreOrder\Model\Creditmemo\Total;

use Magento\Sales\Model\Order\Creditmemo\Total\AbstractTotal;

class Preorder extends AbstractTotal {

	public function collect(\Magento\Sales\Model\Order\Creditmemo $creditmemo) {

		$order = $creditmemo->getOrder();

		$percent = $creditmemo->getSubtotal() / $order->getSubtotal();

		$creditmemo->setPreorder(0);
		$creditmemo->setBasePreorder(0);

		$amount = $creditmemo->getOrder()->getPreorder() * $percent;
		$baseAmount = $creditmemo->getOrder()->getBasePreorder() * $percent;

		$creditmemo->setPreorder($amount);

		$creditmemo->setBasePreorder($baseAmount);

		$creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $amount);
		$creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $baseAmount);

		return $this;
	} 
} 