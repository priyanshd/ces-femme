<?php

namespace Trooperlab\PreOrder\Block\Sales\Totals;

class Preorderitem extends \Magento\Framework\View\Element\Template {

	public function cartItem() 
    {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart          = $objectManager->get('\Magento\Checkout\Model\Cart'); 
        $priceHelper   = $objectManager->create('Magento\Framework\Pricing\Helper\Data'); // Instance of Pricing Helper
        $items         = $cart->getQuote()->getAllVisibleItems(); 
        //$sum           = 0;   
        $preorderArray = array(); 
        foreach($items as $item) 
        {
            $qitem = $objectManager->create('Magento\Quote\Model\Quote\Item')->load($item->getItemId());
            $catalogModel   = $objectManager->get('\Magento\Catalog\Model\Product');
            $product        = $catalogModel->load($item->getProductId());
            if($qitem->getIsPreordered()==1){
                $preorder['name']               = 	$item->getName();
                $preorder['qty']                = 	$item->getQty();
                $price                          =   $item->getRowTotal();
                $preorder['price']              = 	$priceHelper->currency($price,true,false);
                $store                          =   $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();
                $imagewidth=200;
                $imageheight=200;
                $imageHelper  = $objectManager->get('\Magento\Catalog\Helper\Image');
                $image_url = $imageHelper->init($product, 'product_page_image_small')->setImageFile($product->getFile())->resize($imagewidth, $imageheight)->getUrl();
                //echo $image_url
                if(empty($product->getImage())){
                    $image_url = '/pub/static//product/placeholder/thumbnail.jpg';
                }

                //$imageUrl                       =   $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $product->getImage();
                $preorder['image_url']          = 	$image_url;
                $preorder['arrival_date']       =   str_replace(" 00:00:00"," ",$product->getData('arrival_date'));
                $preorderArray[]                =   $preorder;
            }		
       }
       return json_encode($preorderArray);
	}
} 