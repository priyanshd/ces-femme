<?php


namespace Trooperlab\PreOrder\Plugin\Magento\Quote\Model\Quote\Item;

use Magento\Quote\Model\Quote\Item;

class AbstractItem
{ 

    public function aroundGetItemData(
        \Magento\Quote\Model\Quote\Item\AbstractItem $subject,
        \Closure $proceed,
		Item $item
    ) {
		
		$result = $proceed($item);
		return \array_merge(
            ['is_preordered' => (int)$item->getIsPreordered()],
            $result
        );
		
        //Your plugin code
    }
}
