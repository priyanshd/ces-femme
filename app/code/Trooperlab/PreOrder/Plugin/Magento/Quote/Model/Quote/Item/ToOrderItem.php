<?php


namespace Trooperlab\PreOrder\Plugin\Magento\Quote\Model\Quote\Item;

class ToOrderItem
{

    public function aroundConvert(
        \Magento\Quote\Model\Quote\Item\ToOrderItem $subject,
        \Closure $proceed,
		\Magento\Quote\Model\Quote\Item\AbstractItem $item,
		$additional = []	
    ) {
        $orderItem = $proceed($item, $additional);
		$orderItem->setIsPreordered($item->getIsPreordered());
		$orderItem->setArrivalDate($item->getArrivalDate());
		return $orderItem;
    }
}
