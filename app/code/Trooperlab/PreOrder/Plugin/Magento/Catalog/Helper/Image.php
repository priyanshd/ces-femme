<?php


namespace Trooperlab\PreOrder\Plugin\Magento\Catalog\Helper;


class Image
{
	

	protected $_loadingpro;

	protected $_objectManager;
    /**
     * Stock status instance
     *
     * @var \Magento\CatalogInventory\Helper\Stock
     */
    protected $stockHelper;
	protected $_assetRepo;
    /**
     * Store config instance
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
	protected $_logger;
    /**
     * @param \Magento\CatalogInventory\Helper\Stock $stockHelper
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\CatalogInventory\Helper\Stock $stockHelper,
		\Magento\Framework\ObjectManagerInterface $objectManager,
		\Psr\Log\LoggerInterface $logger, //log injection
		\Magento\Framework\View\Asset\Repository $assetRepo,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->stockHelper = $stockHelper;
		$this->_logger = $logger;
		$this->_assetRepo = $assetRepo;
		$this->_objectManager = $objectManager;
        $this->scopeConfig = $scopeConfig;
    }

	
	public function aroundInit(
        \Magento\Catalog\Helper\Image $subject,
        \Closure $proceed,
		$product, $imageId, $attributes = []
    ) {
		$this->_loadingpro = $product;
		$result = $proceed($product, $imageId, $attributes = []);
		return $result;
        //Your plugin code
    }
	
	
    public function aroundGetUrl(
        \Magento\Catalog\Helper\Image $subject,
        \Closure $proceed
    ) {
		 $customerSession = $this->_objectManager->create('Magento\Customer\Model\Session');
		if(!$customerSession->isLoggedIn() && $this->_loadingpro->getHideForGuest() && $this->_loadingpro->getIsPreordered()){
			$placeholderFullPath = 'images/login-for-more-details.jpg';
			return $url = $this->_assetRepo->getUrl($placeholderFullPath);
		}
		$result = $proceed();
		return $result;
        //Your plugin code
    }
}
