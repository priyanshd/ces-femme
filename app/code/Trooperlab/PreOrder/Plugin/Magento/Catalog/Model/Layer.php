<?php


namespace Trooperlab\PreOrder\Plugin\Magento\Catalog\Model;

class Layer
{
	
    protected $_objectManager;
    protected $stockHelper;

    protected $scopeConfig;

    /**
     * @param \Magento\CatalogInventory\Helper\Stock $stockHelper
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\CatalogInventory\Helper\Stock $stockHelper,
		\Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->stockHelper = $stockHelper;
		$this->_objectManager = $objectManager;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Before prepare product collection handler
     *
     * @param \Magento\Catalog\Model\Layer $subject
     * @param \Magento\Catalog\Model\ResourceModel\Collection\AbstractCollection $collection
     *
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforePrepareProductCollection(
        \Magento\Catalog\Model\Layer $subject,
        \Magento\Catalog\Model\ResourceModel\Collection\AbstractCollection $collection
    ) {
		$customerSession = $this->_objectManager->get('\Magento\Customer\Model\Session');
		if(!$customerSession->isLoggedIn()) {
			$collection->addAttributeToFilter('hide_for_guest','0');
		}
    }

}
