<?php


namespace Trooperlab\PreOrder\Plugin\Magento\Rule\Model\Condition\Product;

class AbstractProduct
{

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product
     */
    protected $_productResource;

    protected $_isUsedForRuleProperty = 'is_used_for_promo_rules';

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product $productResource
    ) {
        $this->_productResource = $productResource;
    }

    public function aroundLoadAttributeOptions(
        \Magento\Rule\Model\Condition\Product\AbstractProduct $subject,
        callable $proceed
    ) {
        
        return $return = $proceed();
        // $attributes = $subject->getAttributeOption();
        // $attributes['quote_item_is_preordered'] = __('Pre Order Item in cart');
        // asort($attributes);
        // $subject->setAttributeOption($attributes);
        //Your plugin code
    }

    
    // public function aroundSetAttributeOption(
    //     \Magento\Rule\Model\Condition\Product\AbstractProduct $subject,
    //     callable $proceed,
    //     $attributes
    // ) {
    //     $attributes['quote_item_is_preordered'] = __('Pre Order Item in cart');
    //     return $proceed($attributes);
    //     //Your plugin code
    // }
}