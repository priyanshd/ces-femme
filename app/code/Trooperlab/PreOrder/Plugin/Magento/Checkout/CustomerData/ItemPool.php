<?php


namespace Trooperlab\PreOrder\Plugin\Magento\Checkout\CustomerData;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Quote\Model\Quote\Item;

class ItemPool
{

    public function aroundGetItemData(
        \Magento\Checkout\CustomerData\ItemPool $subject,
        callable $proceed,
		Item $item
    ) {
        $result = $proceed($item);
		return \array_merge(
            ['is_preordered' => (int)$item->getIsPreordered()],
            $result
        );
    }
}
