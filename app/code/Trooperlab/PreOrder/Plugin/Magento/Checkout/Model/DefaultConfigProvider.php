<?php


namespace Trooperlab\PreOrder\Plugin\Magento\Checkout\Model;


use Magento\Checkout\Model\Session as CheckoutSession;


class DefaultConfigProvider
{


	/**
     * @var CheckoutSession
     */
    protected $checkoutSession;
    /**
     * @var \Amasty\Checkout\Helper\Item
     */
    protected $itemHelper;
    /**
     * @var \Magento\Framework\View\LayoutInterface
     */
    protected $layout;

    public function __construct(
        CheckoutSession $checkoutSession,
        \Amasty\Checkout\Helper\Item $itemHelper,
        \Magento\Framework\View\LayoutInterface $layout
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->layout = $layout;
        $this->itemHelper = $itemHelper;
    }
	
    public function afterGetConfig(
        \Magento\Checkout\Model\DefaultConfigProvider $subject,
        $result
    ) {
        $items = $result['totalsData']['items'];

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        for($i=0;$i<count($items);$i++){

            $quoteId = $items[$i]['item_id'];
            $quote = $objectManager->create('\Magento\Quote\Model\Quote\Item')->load($quoteId);
            $productId = $quote->getProductId();
            $product = $objectManager->create('\Magento\Catalog\Model\Product')->load($productId);
            $productFlavours = $product->getResource()->getAttribute('arrival_date')->getFrontend()->getValue($product);         
            if($productFlavours == 'No' || $productFlavours == 'NA'){
                 $productFlavours = '';
            }
            $items[$i]['arrival_date'] = $productFlavours;
        }
		
        $result['totalsData']['items'] = $items;
		
		$quote = $this->checkoutSession->getQuote();

        $preorderItemCollection = array();
        $nonPreorderItemCollection = array();
        
        foreach ($result['quoteItemData'] as &$item) {
            $additionalConfig = $this->itemHelper->getItemOptionsConfig($quote, $item['item_id']);

            if (!empty($additionalConfig)) {
                $item['amcheckout'] = $additionalConfig;
            }
            $productId = $item['item_id'];
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $qitem = $objectManager->create('Magento\Quote\Model\Quote\Item')->load($productId);
            if($qitem->getIsPreordered()==1){
				$item['arrivaldate']            = $qitem->getArrivalDate();
                $item['preorder']               = 'Pre-Order';
                $preorderItemCollection[] = $item;
            }else{
                $item['arrivaldate']            = 'Now';	
                $nonPreorderItemCollection[] = $item; 
                
            }
        }

        $result['preorderItemCollection'] = $preorderItemCollection;
        $result['nonPreorderItemCollection'] = $nonPreorderItemCollection;
        $result['preorderItemCount'] = count($preorderItemCollection);
        $result['nonPreorderItemCount'] = count($nonPreorderItemCollection);
		
        return $result;
    }
}
