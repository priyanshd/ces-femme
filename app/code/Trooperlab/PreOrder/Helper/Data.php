<?php

namespace Trooperlab\PreOrder\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper {

	
	const CONFIG_CUSTOM_IS_ENABLED_PREORDER = 'preorder_customfee/preorder_customfee/preorder_status';
	const CONFIG_CUSTOM_FEE_PREORDER = 'preorder_customfee/preorder_customfee/preorder_customfeeamount';
	const CONFIG_FEE_LABEL_PREORDER = 'preorder_customfee/preorder_customfee/preorder_name';
	const CONFIG_MINIMUM_ORDER_AMOUNT_PREORDER = 'preorder_customfee/preorder_customfee/preorder_minimumorderamount';

	public function isModuleEnabledPreorder() {
		$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
		$isEnabled = $this->scopeConfig->getValue(self::CONFIG_CUSTOM_IS_ENABLED_PREORDER, $storeScope);
		return $isEnabled;
	} 

	public function getCustomFeePreorder() {
		$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
		$fee = $this->scopeConfig->getValue(self::CONFIG_CUSTOM_FEE_PREORDER, $storeScope);
		return $fee;
	} 

	public function getFeeLabelPreorder() {
		$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
		$feeLabel = $this->scopeConfig->getValue(self::CONFIG_FEE_LABEL_PREORDER, $storeScope);
		return $feeLabel;
	} 

	public function getMinimumOrderAmountPreorder() {
		$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
		$MinimumOrderAmount = $this->scopeConfig->getValue(self::CONFIG_MINIMUM_ORDER_AMOUNT_PREORDER, $storeScope);
		return $MinimumOrderAmount;
	} 
	

} 
