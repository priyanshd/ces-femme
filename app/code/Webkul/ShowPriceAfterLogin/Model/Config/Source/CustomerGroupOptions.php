<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_ShowPriceAfterLogin
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\ShowPriceAfterLogin\Model\Config\Source;

class CustomerGroupOptions implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface $objectmanager
     */
    protected $_objectManager;

    /**
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectmanager
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectmanager
    ) {
    
        $this->_objectManager = $objectmanager;
    }
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return $this->getGroupsLists();
    }

    /**
     * Type of customer groups
     * @return array
     */
    public function getGroupsLists()
    {
        $groupOptions = $this->_objectManager->get('\Magento\Customer\Model\Customer\Attribute\Source\Group')->getAllOptions();
        return $groupOptions;
    }
}
