<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_ShowPriceAfterLogin
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\ShowPriceAfterLogin\Model\Config\Source;

class CategoryListOptions implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface $objectmanager
     */
    protected $_objectManager;

    /**
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectmanager
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectmanager
    ) {
    
        $this->_objectManager = $objectmanager;
    }
    /**
     * Options getter.
     *
     * @return array
     */
    public function toOptionArray()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $collection = $objectManager->create('Magento\Catalog\Model\Category')->getCollection();
        $collection->addAttributeToSelect("name");
        $collection->addAttributeToFilter("level", ["gteq" => 2]);
        $firstLevel = [];
        $secondLevel = [];
        $thirdLevel = [];
        $allCategories = [];
        $data = [];
        $data[] = ['value'=>'all cat','label'=>'All Categories'];
        foreach ($collection as $category) {
            $parentId = $category->getParentId();
            $id = $category->getEntityId();
            $level = $category->getLevel();
            if ($level == 2) {
                $firstLevel[$parentId][] = $id;
            }
            if ($level == 3) {
                $secondLevel[$parentId][] = $id;
            }
            if ($level == 4) {
                $thirdLevel[$parentId][] = $id;
            }
            $allCategories[$id] = $category->getName();
        }
        foreach ($firstLevel as $flp => $flc) {
            foreach ($flc as $slc) {
                $data[] = ['value' => $slc, 'label' => $allCategories[$slc]];
                if (array_key_exists($slc, $secondLevel)) {
                    foreach ($secondLevel[$slc] as $slcId) {
                        $data[] = ['value' => $slcId, 'label' => "- ".$allCategories[$slcId]];
                        if (array_key_exists($slcId, $thirdLevel)) {
                            foreach ($thirdLevel[$slcId] as $tlcId) {
                                $data[] = ['value' => $tlcId, 'label' => "-- ".$allCategories[$tlcId]];
                            }
                        }
                    }
                }
            }
        }
        return $data;
    }
}
