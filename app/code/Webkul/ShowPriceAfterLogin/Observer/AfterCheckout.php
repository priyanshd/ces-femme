<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_ShowPriceAfterLogin
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\ShowPriceAfterLogin\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class AfterCheckout implements ObserverInterface
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Webkul\ShowPriceAfterLogin\Helper\Data
     */
    protected $_helper;
    
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;
    
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_url;
    
    /**
     * @var \Magento\Checkout\Model\Cart
     */
    public $cart;

    /**
     * __construct function
     *
     * @param \Magento\Framework\ObjectManagerInterface   $objectManager
     * @param \Webkul\ShowPriceAfterLogin\Helper\Data     $helper
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Checkout\Model\Cart                $cart
     * @param \Magento\Framework\UrlInterface             $url
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Webkul\ShowPriceAfterLogin\Helper\Data $helper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\UrlInterface $url
    ) {
  
        $this->_objectManager = $objectManager;
        $this->_helper = $helper;
        $this->_messageManager = $messageManager;
        $this->_url = $url;
        $this->cart = $cart;
    }

    /**
     * Observer to stop from checkout if customer is logged out or not allowed.
     */
    public function execute(Observer $observer)
    {
        if (!$this->_helper->storeAvilability()) {
            return true;
        }
        $flag = 0;
        foreach ($this->cart->getQuote()->getItemsCollection() as $item) {
            if ($this->_helper->statusOfCategorySettingForAllUser() && $this->_helper->isAllowedForGuestUser($item->getProduct())) {
                continue;
            } else {
                    $productModel = $item->getProduct();
                    $showPriceCustomerGroup = $productModel->getShowPriceCustomerGroup();
                if ($productModel->getData('show_price') && $this->_helper->configPriority() == "product_configuration") {
                    $isAllowedCustomerGroups = $this->_helper->isAllowedCustomerGroupsForParticularProduct($showPriceCustomerGroup);
                } else {
                    $isAllowedCustomerGroups = $this->_helper->isAllowedCustomerGroups();
                }
                if ($this->_helper->storeAvilability() && !$isAllowedCustomerGroups) {
                    $flag = 1;
                    // $this->deleteQuoteItems();
                    $this->_messageManager->addError(__("Login customer not allowed to purchase %1 itemss!", $productModel->getName()));
                }
            }
        }
        if ($flag) {
            $CustomRedirectionUrl = $this->_url->getUrl('/');
            $observer->getControllerAction()
                ->getResponse()
                ->setRedirect($CustomRedirectionUrl);
        }
    }
   
   /**
    * deleteQuoteItems function delete the items from cart
    *
    * @return void
    */
    public function deleteQuoteItems()
    {
        $checkoutSession = $this->getCheckoutSession();
        $allItems = $checkoutSession->getQuote()->getAllVisibleItems();//returns all the items in session
        foreach ($allItems as $item) {
            $itemId = $item->getItemId();//item id of particular item
            $quoteItem=$this->getItemModel()->load($itemId);//load particular item which you want to delete by his item id
            $quoteItem->delete();//deletes the item
        }
    }

    /**
     * getCheckoutSession function get the object of checkout session
     *
     * @return \Magento\Checkout\Model\Session
     */
    public function getCheckoutSession()
    {
        $checkoutSession = $this->_objectManager->get('Magento\Checkout\Model\Session');//checkout session
        return $checkoutSession;
    }

    /**
     * getItemModel function get the object of the quote
     *
     * @return \Magento\Quote\Model\Quote\Item
     */
    public function getItemModel()
    {
        $itemModel = $this->_objectManager->create('Magento\Quote\Model\Quote\Item');//Quote item model to load quote item
        return $itemModel;
    }
}
