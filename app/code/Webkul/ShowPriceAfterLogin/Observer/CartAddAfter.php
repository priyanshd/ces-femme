<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_ShowPriceAfterLogin
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\ShowPriceAfterLogin\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;

class CartAddAfter implements ObserverInterface
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * customer session
     * @var Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Webkul\ShowPriceAfterLogin\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_catalaogProduct;
    
    /**
     * __construct function
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Customer\Model\Session           $customerSession
     * @param \Webkul\ShowPriceAfterLogin\Helper\Data   $helper
     * @param \Magento\Catalog\Model\ProductFactory     $catalaogProduct
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Customer\Model\Session $customerSession,
        \Webkul\ShowPriceAfterLogin\Helper\Data $helper,
        \Magento\Catalog\Model\ProductFactory $catalaogProduct
    ) {
    
        $this->_objectManager = $objectManager;
        $this->_customerSession = $customerSession;
        $this->_helper = $helper;
        $this->_catalaogProduct = $catalaogProduct;
    }

    /**
     * Observer to stop product from adding to cart.
     */
    public function execute(Observer $observer)
    {
        $item = $observer->getEvent()->getQuoteItem();
        $productId = $item->getProductId();
        $productModel = $this->_catalaogProduct->create()->load($productId);
        if (!$this->_helper->storeAvilability()) {
            return true;
        }
        if ($this->_helper->isAllowedForGuestUser($productModel)) {
            return true;
        }
        $showPriceCustomerGroup = $productModel->getShowPriceCustomerGroup();
        if ($productModel->getData('show_price') && $this->_helper->configPriority() == "product_configuration") {
            $isAllowedCustomerGroups = $this->_helper->isAllowedCustomerGroupsForParticularProduct($showPriceCustomerGroup);
        } else {
            $isAllowedCustomerGroups = $this->_helper->isAllowedCustomerGroups();
        }
        if ($productId) {
            $status = $this->_customerSession->isLoggedIn();
            if ($this->_helper->storeAvilability()) {
                if (!$status || !$isAllowedCustomerGroups) {
                    if (!$status) {
                        try {
                            throw new LocalizedException(__('Please login to purchase items'));
                        } catch (Exception $e) {
                            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
                        }
                    }
                    if ($status && !$isAllowedCustomerGroups) {
                        try {
                            throw new LocalizedException(__('Login customer not allowed to purchase itemssdfsdf!'));
                        } catch (Exception $e) {
                            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
                        }
                    }
                }
            }
        }
    }
}
