<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_ShowPriceAfterLogin
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\ShowPriceAfterLogin\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class OnProductView implements ObserverInterface
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Webkul\ShowPriceAfterLogin\Helper\Data
     */
    protected $_helper;
    
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * __construct function
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Webkul\ShowPriceAfterLogin\Helper\Data   $helper
     * @param \Magento\Framework\Registry               $registry
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Webkul\ShowPriceAfterLogin\Helper\Data $helper,
        \Magento\Framework\Registry $registry
    ) {
    
        $this->_objectManager = $objectManager;
        $this->_helper = $helper;
        $this->_registry = $registry;
    }

    /**
     * Observer to disable product price if customer is logged out .
     */
    public function execute(Observer $observer)
    {
        if (!$this->_helper->storeAvilability()) {
            return true;
        }
        $status = $this->_objectManager
        ->create('Magento\Customer\Model\Session')
        ->isLoggedIn();
        $callForPriceAttributes = $this->getModuleAttributeForParticularProduct();
        $isAllowedCustomerGroups = $this->_helper->isAllowedCustomerGroups();
        $productCustomerGroup = $this->_helper->isAllowedCustomerGroupsForParticularProduct(
            $callForPriceAttributes['show_price_customer_group']
        );
        if ($callForPriceAttributes['show_price'] == 1 && $this->_helper->configPriority() == "product_configuration") {
            if (!$status || !$productCustomerGroup) {
                $block = $observer->getBlock();
                if ($block->getType() == 'Magento\Catalog\Pricing\Render'
                    || $block->getType() == 'Magento\Framework\Pricing\Render') {
                    $block->setTemplate(false);
                }
            }
        } else {
            if ($this->_helper->storeAvilability()) {
                if (!$status || !$isAllowedCustomerGroups) {
                    $block = $observer->getBlock();
                    if ($block->getType() == 'Magento\Catalog\Pricing\Render'
                        || $block->getType() == 'Magento\Framework\Pricing\Render') {
                        $block->setTemplate(false);
                    }
                }
            }
        }
    }

    /**
     * getAllShowPriceAfterLoginModuleAttribute function return the aatribute value related to ShowPrixeAfterLogin module
     * for particular product
     *
     * @return array
     */
    public function getModuleAttributeForParticularProduct()
    {
        $registry = $this->_registry->registry('product');
        if (isset($registry)) {
            $attribute['show_price'] = isset(
                $this->_registry->registry('product')->getData()['show_price']
            )?$this->_registry->registry('product')->getData()['show_price']:"";

            $attribute['call_for_price'] = isset(
                $this->_registry->registry('product')->getData()['call_for_price']
            )?$this->_registry->registry('product')->getData()['call_for_price']:"";

            $attribute['show_price_customer_group'] = isset(
                $this->_registry->registry('product')->getData()['show_price_customer_group']
            )?$this->_registry->registry('product')->getData()['show_price_customer_group']:"";

            $attribute['call_for_price_label'] = isset(
                $this->_registry->registry('product')->getData()['call_for_price_label']
            )?$this->_registry->registry('product')
                                                 ->getData()['call_for_price_label']:"";
            $attribute['call_for_price_link'] = isset(
                $this->_registry->registry('product')->getData()['call_for_price_link']
            )?$this->_registry->registry('product')->getData()['call_for_price_link']:"";

            return $attribute;
        }
    }
}
