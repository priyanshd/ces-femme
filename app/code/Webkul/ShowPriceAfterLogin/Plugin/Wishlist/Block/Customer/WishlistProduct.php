<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_ShowPriceAfterLogin
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\ShowPriceAfterLogin\Plugin\Wishlist\Block\Customer;

use Magento\Framework\Pricing\Render;

class WishlistProduct
{

    /**
     * object manager for injecting objects.
     *
     * @var Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlInterface;

    /**
     * @var \Webkul\ShowPriceAfterLogin\Helper\Data
     */
    protected $_helper;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\UrlInterface           $urlinterface
     * @param \Webkul\ShowPriceAfterLogin\Helper\Data   $helper
     */
    public function __construct(
        \Webkul\ShowPriceAfterLogin\Helper\Data $helper,
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
    
    
        $this->_helper = $helper;
        $this->_urlInterface = $urlInterface;
        $this->_objectManager = $objectManager;
    }

    /**
     * function to run to change the return data of GetProductPriceHtml.
     *
     * @param \Magento\Wishlist\Block\Customer\Sidebar $list
     * @param Closure $proceed
     * @param \Magento\Catalog\Model\Product] $product
     * @param string $priceType
     * @param string $renderZone
     * @param array $arguments
     * @return string
     */
    public function aroundGetProductPriceHtml(
        \Magento\Wishlist\Block\Customer\Sidebar $list,
        $proceed,
        $product,
        $priceType,
        $renderZone = Render::ZONE_ITEM_LIST,
        array $arguments = []
    ) {
    
        if (!$this->_helper->storeAvilability()) {
            return $proceed($product, $priceType, $renderZone,$arguments);
        }
        if ($this->_helper->isAllowedForGuestUser($product)) {
            return $proceed($product, $priceType, $renderZone,$arguments);
        }
        $customerSession = $this->_objectManager->create('Magento\Customer\Model\Session');
        $productCustomerGroup = $product->getData('show_price_customer_group');
        if ($product->getData('show_price') && $this->_helper->configPriority() == "product_configuration") {
            if ($customerSession->isLoggedIn()) {
                if ($this->_helper->isAllowedCustomerGroupsForParticularProduct($productCustomerGroup)) {
                    return $proceed($product, $priceType, $renderZone,$arguments);
                } else {
                    if ($product->getData('call_for_price')) {
                        $label = $this->escapeHtmlEntites($product->getData('call_for_price_label'));
                        if ($label == "") {
                            $label = "Not allowed to view price";
                        }
                        $link  = $product->getData('call_for_price_link');
                        $link = $this->_helper->checkUrl($link);
                        if ($link == "") {
                            $link = $this->_urlInterface->getUrl('contact');
                        }
                        return '<span class="wkshowcallforpriceforwishlist" data-label="'.$label.'" data-link="'.$link.'"></span>';
                    } else {
                        $label = "Logged in customer group not<br>allowed to View Price";
                        $link  = "customer-not-allowed";
                        return '<span class="wkshowcallforpriceforwishlist" data-label="'.$label.'" data-link="'.$link.'"></span>';
                    }
                }
            } else {
                if ($product->getData('call_for_price')) {
                    $label = $this->escapeHtmlEntites($product->getData('call_for_price_label'));
                    if ($label == "") {
                        $label = "Not allowed to view price";
                    }
                    $link  = $product->getData('call_for_price_link');
                    $link = $this->_helper->checkUrl($link);
                    if ($link == "") {
                        $link = $this->_urlInterface->getUrl('contact');
                    }
                    return '<span class="wkshowcallforpriceforwishlist" data-label="'.$label.'" data-link="'.$link.'"></span>';
                } else {
                    $label = 'Please Login To View Price';
                    $link  = "log-in";
                    return '<span class="wkshowcallforpriceforwishlist" data-label="'.$label.'" data-link="'.$link.'"></span>';
                }
            }
        } else {
            if (($this->_helper->storeAvilability() && !$customerSession->isLoggedIn())) {
                if ($this->_helper->isCallForPriceConfigSetting() == 1) {
                    $label = $this->escapeHtmlEntites($this->_helper->callForPriceConfigLabel());
                    if ($label == "") {
                        $label = "Not allowed to view price";
                    }
                    $link = $this->_helper->callForPriceConfigLink();
                    $link = $this->_helper->checkUrl($link);
                    if ($link == "") {
                        $link = $this->_urlInterface->getUrl('contact');
                    }
                    return '<span class="wkshowcallforpriceforwishlist" data-label="'.$label.'" data-link="'.$link.'"></span>';
                } else {
                    return '<span style="display:block;margin:10px 0" class="wkremovepriceandcartforwishlist"></span>';
                }
            } else {
                if ($this->_helper->isAllowedCustomerGroups()) {
                        return $proceed($product, $priceType, $renderZone,$arguments);
                } else {
                    if ($this->_helper->isCallForPriceConfigSetting() == 1) {
                        $label = $this->escapeHtmlEntites($this->_helper->callForPriceConfigLabel());
                        if ($label == "") {
                            $label = "Not allowed to view price";
                        }
                        $link = $this->_helper->callForPriceConfigLink();
                        $link = $this->_helper->checkUrl($link);
                        if ($link == "") {
                            $link = $this->_urlInterface->getUrl('contact');
                        }
                        return '<span class="wkshowcallforpriceforwishlist" data-label="'.$label.'" data-link="'.$link.'"></span>';
                    } else {
                        return '<span class="wkremovepriceandcartforwishlist"></span>';
                    }
                }
            }
        }
    }

    /**
     * escapeHtmlEntites function escape the html tags in a string
     *
     * @param string $value
     * @return string
     */
    public function escapeHtmlEntites($value)
    {
        return htmlentities(htmlentities($value));
    }
}
