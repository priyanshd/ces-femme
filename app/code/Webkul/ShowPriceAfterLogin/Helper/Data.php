<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_ShowPriceAfterLogin
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\ShowPriceAfterLogin\Helper;

use Magento\Customer\Model\Session;

/**
 * ShowPriceAfterLogin data helper.
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Customer session.
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @param Session  $customerSession
     * @param Magento\Framework\App\Helper\Context        $context
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        Session $customerSession,
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        $this->_customerSession = $customerSession;
        $this->_objectManager = $objectManager;
        $this->_urlInterface = $context->getUrlBuilder();
        parent::__construct($context);
    }

    /**
     * get storeAvilability that weather it is,
     * enable or disable from configuration.
     *
     * @return int
     */
    public function storeAvilability()
    {
               return $this->scopeConfig
                          ->getValue(
                              'webkulShowPriceAfterLogin/storeAbility/ebableOrDisableStoreField',
                              \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                          );
    }

    /**
     * return the allowed customer group from config to see the price of product
     *
     * @return int
     */
    public function getListsOfCustomerGroupToGrantAcess()
    {
                return $this->scopeConfig
                            ->getValue(
                                'webkulShowPriceAfterLogin/storeAbility/customerGroupList',
                                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                            );
    }

    /**
     * return the status that call for price option from config is set or not
     *
     * @return int
     */
    public function isAllowedCustomerGroups()
    {
        $listOfGroupsFromConfig = $this->getListsOfCustomerGroupToGrantAcess();
        $loggedInUserGroup =  $this->_customerSession->getCustomer()->getGroupId();
        if (in_array($loggedInUserGroup, explode(',', $listOfGroupsFromConfig))) {
            return 1;
        }
        return 0;
    }
    public function isCallForPriceConfigSetting()
    {
                return $this->scopeConfig
                    ->getValue(
                        'webkulShowPriceAfterLogin/callForPrice/callForPriceEnable',
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                    );
    }

    /**
     * get the call for price label from config.
     *
     * @return string
     */
    public function callForPriceConfigLabel()
    {
                return $this->scopeConfig
                    ->getValue(
                        'webkulShowPriceAfterLogin/callForPrice/callForPriceTitle',
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                    );
    }

    /**
     * get the call for price link from config
     *
     * @return string
     */
    public function callForPriceConfigLink()
    {
                return $this->scopeConfig
                    ->getValue(
                        'webkulShowPriceAfterLogin/callForPrice/callForPriceLink',
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                    );
    }
    
    /**
     * statusOfCategorySettingForAllUser function get the status of category setting for all user
     *
     * @return boolean
     */
    public function statusOfCategorySettingForAllUser()
    {
                return $this->scopeConfig
                    ->getValue(
                        'webkulShowPriceAfterLogin/storeAbility/ebableOrDisableCategory',
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                    );
    }

    /**
     * getListOfCategories function get the list of categories allowed from backend for all user
     *
     * @return array
     */
    public function getListOfCategories()
    {
                return $this->scopeConfig
                    ->getValue(
                        'webkulShowPriceAfterLogin/storeAbility/categoryList',
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                    );
    }

    /**
     * isAllowedForGuestUser function check weather the category of product is allowed for selected categories from backend
     *
     * @param [Magento/Catlog/Product/Model] $product
     * @return boolean
     */
    public function isAllowedForGuestUser($product = null)
    {
        if ($this->statusOfCategorySettingForAllUser()) {
            $allowedCategoriesLists = $this->getListOfCategories();
            if (strpos($allowedCategoriesLists, 'all cat') !== false) {
                return true;
            }
            $cat_id = $product->getCategoryIds();
            $result = array_intersect(explode(',', $allowedCategoriesLists), $cat_id);
            if (!empty($result)) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    /**
     * return the status that logged in customer group and product
     * attribute customer group are same or not?
     *
     * @return int
     */
    public function isAllowedCustomerGroupsForParticularProduct($productCustomerGroupAttribute)
    {
        $array = explode(',', $productCustomerGroupAttribute);
        $loggedInUserGroup =  $this->_customerSession->getCustomer()->getGroupId();
        // if ($productCustomerGroupAttribute == $loggedInUserGroup) {
        if (in_array($loggedInUserGroup, $array)) {
            return 1;
        }
        return 0;
    }

    /**
     * get all list of group attribute of customer to show on particular product page
     *
     * @return Array
     */
    public function getGroupsLists()
    {
        $groupOptions = $this->_objectManager->
                                get('\Magento\Customer\Model\Customer\Attribute\Source\Group')->getAllOptions();
        return $groupOptions;
    }

    /**
     * get priority that weather it is configuration
     * setting or individual product setting.
     *
     * @return string
     */
    public function configPriority()
    {
               return $this->scopeConfig
                          ->getValue(
                              'webkulShowPriceAfterLogin/callForPriceConfigPriority/callForPriceConfigPriorityEnable',
                              \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                          );
    }

    /**
     * checkUrl function check the link is relative url or not
     *
     * @param string $link
     * @return string
     */
    public function checkUrl($link)
    {
        if ($link == "") {
            return "";
        }
        $urlParts = parse_url($link);
        if (isset(
            $urlParts['scheme']
        ) &&
        ($urlParts['scheme'] == 'http' || $urlParts['scheme'] == 'https') &&
        isset($urlParts['host'])
        ) {
            return $link;
        } else {
            return $this->_urlInterface->getUrl($link);
        }
    }
}
