<?php
 /**
  * Webkul_ShowPriceAfterLogin Module Registration
  *
  * @category    Webkul
  * @package     Webkul_ShowPriceAfterLogin
  * @author      Webkul Software Private Limited
  *
  */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Webkul_ShowPriceAfterLogin',
    __DIR__
);
