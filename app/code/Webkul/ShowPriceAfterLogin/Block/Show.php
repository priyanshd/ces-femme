<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_ShowPriceAfterLogin
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\ShowPriceAfterLogin\Block;

use Magento\Store\Model\Store;
use Magento\Framework\Stdlib\DateTime\DateTime;

/**
 * ShowPriceAfterLogin block.
 *
 * @author      Webkul Software
 */
class Show extends \Magento\Framework\View\Element\Template
{

    /**
     * @var Session
     */
    protected $_customerSession;

    /**
     * Magento\Framework\Registry.
     *
     * @var [type]
     */
    protected $_registry;

    /**
     * Customer session
     *
     * @var \Magento\Framework\App\Http\Context
     */
    protected $_httpContext;

    /**
     * @var \Webkul\ShowPriceAfterLogin\Helper\Data
     */
    protected $_helper;

    /**
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Session                  $_customerSession
     * @param \Magento\Framework\Registry                      $_registry
     * @param \Webkul\ShowPriceAfterLogin\Helper\Data          $helper
     * @param DateTime                                         $date
     * @param Store                                            $store
     * @param array                                            $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $_customerSession,
        \Magento\Framework\Registry $_registry,
        \Magento\Framework\App\Http\Context $httpContext,
        \Webkul\ShowPriceAfterLogin\Helper\Data $helper,
        DateTime $date,
        Store $store,
        array $data = []
    ) {
    
    
        $this->_customerSession = $_customerSession;
        $this->_registry = $_registry;
        $this->_helper = $helper;
        parent::__construct($context, $data);
        $this->_httpContext = $httpContext;
    }

    /**
     * get storeAvilability that weather it is,
     * enable or disable from configuration.
     *
     * @return int
     */
    public function isStoreAvilable()
    {
        return  $this->_helper->storeAvilability();
    }

    /**
     * get the selected group of list from config.
     *
     * @return Array
     */
    public function getGroupLists()
    {
        return  $this->_helper->getListsOfCustomerGroupToGrantAcess();
    }

    /**
     * get priority that weather it is configuration
     * setting or individual product setting.
     *
     * @return string
     */
    public function configPriority()
    {
        return  $this->_helper->configPriority();
    }

    /**
     * return all the attribute of product related to show price.
     *
     * @return Array
     */
    public function getAllShowPriceAfterLoginModuleAttribute()
    {
        $attribute['show_price'] = isset(
            $this->_registry->registry('product')->getData()['show_price']
        )?$this->_registry->registry('product')->getData()['show_price']:"";

        $attribute['call_for_price'] = isset(
            $this->_registry->registry('product')->getData()['call_for_price']
        )?$this->_registry->registry('product')->getData()['call_for_price']:"";

        $attribute['show_price_customer_group'] = isset(
            $this->_registry->registry('product')->getData()['show_price_customer_group']
        )?$this->_registry->registry('product')->getData()['show_price_customer_group']:"";

        $attribute['call_for_price_label'] = isset(
            $this->_registry->registry('product')->getData()['call_for_price_label']
        )?$this->_registry->registry('product')
                                             ->getData()['call_for_price_label']:"";
        $attribute['call_for_price_link'] = isset(
            $this->_registry->registry('product')->getData()['call_for_price_link']
        )?$this->_registry->registry('product')->getData()['call_for_price_link']:"";

        return $attribute;
    }

    /**
     * return the status that logged in customer group and product
     * attribute customer group are same or not?
     *
     * @return int
     */
    public function isAllowedCustomerGroupsForParticularProduct($productCustomerGroupAttribute = null)
    {
        return $this->_helper->isAllowedCustomerGroupsForParticularProduct($productCustomerGroupAttribute);
    }

    /**
     * get the call for price label from config.
     *
     * @return string
     */
    public function callForPriceConfigLabel()
    {
        return $this->_helper->callForPriceConfigLabel();
    }

    /**
     * get the call for price link from config
     *
     * @return string
     */
    public function callForPriceConfigLink()
    {
        return $this->_helper->callForPriceConfigLink();
    }

    /**
     * return the status that call for price option from config is set or not
     *
     * @return int
     */
    public function isCallForPriceConfigSetting()
    {
        return $this->_helper->isCallForPriceConfigSetting();
    }

    /**
     * get add to cart label
     *
     * @return string
     */
    public function getLabel()
    {
        if ($this->isCustomerLoggedIn()) {
            return __('Logged in customer group not<br>allowed to View Price');
        } else {
            return __('Login To View Price');
        }
    }

    /**
     * check if customer logged in
     * @return boolean
     */
    public function isCustomerLoggedIn()
    {
        return $this->_customerSession->isLoggedIn();
    }

    /**
     * getlogged in customer group ID
     *
     * @return int
     */
    public function getCustomerGroupId()
    {
        return $this->_customerSession->getCustomer()->getGroupId();
    }

    /**
     * get redirection url
     *
     * @return string
     */
    public function setRedirectReferer()
    {
        $this->_customerSession->setBeforeAuthUrl($this->_urlBuilder->getCurrentUrl());
    }

    /**
     * checkUrl function check the url is relative or not ?
     *
     * @param string $link
     * @return string
     */
    public function checkUrl($link)
    {
        return $this->_helper->checkUrl($link);
    }
    /**
     * getProduct function get the product model
     *
     * @return Magento/Catalog/Product/Model
     */
    public function getProduct()
    {
        return $this->_registry->registry('product');
    }
}
