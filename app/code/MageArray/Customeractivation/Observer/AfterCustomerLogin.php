<?php

namespace MageArray\Customeractivation\Observer;

use MageArray\Customeractivation\Helper\Data as DataHelper;
use Magento\Customer\Model\Session;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Message\ManagerInterface;

/**
 * Class AfterCustomerLogin
 * @package MageArray\Customeractivation\Observer
 */
class AfterCustomerLogin implements ObserverInterface
{
    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var DataHelper
     */
    private $dataHelper;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * AfterCustomerLogin constructor.
     * @param DataHelper $dataHelper
     * @param ManagerInterface $messageManager
     * @param Session $customerSession
     */
    public function __construct(
        DataHelper $dataHelper,
        ManagerInterface $messageManager,
        Session $customerSession
    ) {
        $this->customerSession = $customerSession;
        $this->dataHelper = $dataHelper;
        $this->messageManager = $messageManager;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if (!$this->dataHelper->isActive()) {
            return;
        }
        $customer = $observer->getCustomer();

        if ($this->dataHelper->isCustomerActivationByGroup()
            && !in_array(
                $customer->getGroupId(),
                $this->dataHelper
                    ->getCustomerActivationGroupIds()
            )
        ) {
            return;
        }

        $statusValue = $this->dataHelper
                        ->getAttributeValue($customer->getId());


        if (!$statusValue ) {
            $this->customerSession->setId(null);
            $errorMessage = $this->dataHelper
                ->getErrorMessageForUser();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $time = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
             
            $to_time = strtotime($time->gmtDate());
            $from_time = strtotime($customer->getCreatedAt());
            $differnet = (int)round(abs($to_time - $from_time) / 60,2);
            if($differnet > 3){
                $this->messageManager->addError(__($errorMessage));
            }
        }
    }
}
