/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'mage/smart-keyboard-handler',
    'mage/mage',
    'mage/ie-class-fixer',
    'domReady!'
], function ($, keyboardHandler) {
    'use strict';

    if ($('body').hasClass('checkout-cart-index')) {
        if ($('#co-shipping-method-form .fieldset.rates').length > 0 &&
            $('#co-shipping-method-form .fieldset.rates :checked').length === 0
        ) {
            $('#block-shipping').on('collapsiblecreate', function () {
                $('#block-shipping').collapsible('forceActivate');
            });
        }
    }

    $('.cart-summary').mage('sticky', {
        container: '#maincontent'
    });

    $('.panel.header > .header.links').clone().appendTo('#store\\.links');

    var pageheader = $('.page-header').height();
    var navsections = $('.nav-sections').height();
    var pageNav = pageheader + navsections;

    $(window).scroll(function() {
        if ($(this).scrollTop() > pageNav){
            $('.page-wrapper').addClass('sticky-header');
        }else{
            $('.page-wrapper').removeClass('sticky-header');
        }
    });

    $( "body" ).on( "click", ".action.showcart", function() { //  .block-minicart .subtotal
        var blockminicart = $('.block-minicart').height();
        var quickcarttop = $('.quickcart-top').height();
        var subtotal = $('.block-minicart .subtotal').height();
        var actions = $('.block-minicart .block-content > .actions').height();
        var minusHeight = (quickcarttop+subtotal+actions);
    });

    keyboardHandler.apply();
});
